import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { AngularFirestore} from '@angular/fire/firestore';
import { AuthService } from '../services/auth.service';
import { AlertController, Platform, ModalController} from '@ionic/angular';
import { InAppPurchase2, IAPProduct } from '@ionic-native/in-app-purchase-2/ngx';

@Component({
  selector: 'app-new',
  templateUrl: './new.page.html',
  styleUrls: ['./new.page.scss'],
})
export class NewPage implements OnInit {
   productsPurchased = []
   allLevelsID
   allLevelsPrice
   allLevelsOwnedStatus
   allLevelsArray
   instructionalID
   instructionalPrice
   instructionalOwnedStatus
   instructionalArray
   quizID
   quizPrice
   quizOwnedStatus
   quizArray
   turnID
  turnPrice
  turnOwnedStatus
  turnArray
  levelPicked
  showBuyBtns: boolean = false
  purchasedItem: boolean = true
  saveDollarAmount
  regularValue
  OFFICIAL_PROD_SELECTED
  //display card
  showCardTurns: boolean = true
  showCardLevels: boolean = true
  showCardEntireApp: boolean = true
  //display buy button 
  showBuyBtnTurns: boolean = true
  showBuyBtnLevels: boolean = false
  showBuyBtnEntireApp: boolean = true
  //display purcahse check mark
  showCheckTurns: boolean = false
  showCheckLevels: boolean = false
  showCheckEntireApp: boolean = false
  isIpad = false 

  constructor(
    public af: AngularFirestore,
    private authService: AuthService,
    private plt: Platform,
    public alertController: AlertController,
    public modalCtrl: ModalController,
    private store: InAppPurchase2,
    private ref: ChangeDetectorRef
    ) { 
      // this.productsPurchased.push('com.movesofficial.alllevels');
      // localStorage.setItem('productsPurchased', JSON.stringify(this.productsPurchased));
      console.log = function(){};
      console.log('in dana new page');

      this.showPurchasedCheckMark()

    this.authService.passAllLevels$.subscribe((allLevelsID) => {
      this.allLevelsArray = JSON.parse(allLevelsID)
      this.allLevelsID = this.allLevelsArray[0].prodID
      this.allLevelsPrice = this.allLevelsArray[0].price
      this.allLevelsOwnedStatus = this.allLevelsArray[0].owned
      console.log('dana all levels assigning', this.allLevelsID);
      
     })

     this.authService.passInstructionalInfo$.subscribe((instructionalID) => {
      this.instructionalArray = JSON.parse(instructionalID)
     })

     this.authService.passTurnChecklist$.subscribe((turnChecklistID) => {
      this.turnArray = JSON.parse(turnChecklistID)
      this.turnID = this.turnArray[0].prodID
      this.turnPrice = this.turnArray[0].price
      this.turnOwnedStatus = this.turnArray[0].owned
      console.log('dana  turnID assigning', this.turnID);
     })
  }

  ngOnInit() {
    this.authService.passAllLevels()
    this.authService.passInstructionalInfo()
    this.authService.passTurnChecklist()

    this.af.collection("Links").doc('save').ref.get().then((doc) =>{
      console.log(doc.data()['save'], 'doc.data()[s');
      this.saveDollarAmount = doc.data()['save'];
      this.regularValue = doc.data()['regularValue'];
    });
  }

  selectLevel(){
    this.showCheckLevels = false
    this.productsPurchased = JSON.parse(localStorage.getItem('productsPurchased'))
    console.log(this.productsPurchased, 'dana products purchased in select level function');
    console.log('in select level funtion #1');
     //show price on buttons 
     console.log(this.levelPicked, 'levelPicked #2')
    console.log('com.movesofficial.' + (this.levelPicked.replace(/\s/g, "")).replace(/-/g, "").toLowerCase() + 'instructional', 'this is id official #3');
    if ((this.productsPurchased != null) && (this.productsPurchased != undefined)) {
    //if own level 
    if((this.productsPurchased.indexOf('com.movesofficial.' + (this.levelPicked.replace(/\s/g, "")).replace(/-/g, "").toLowerCase() + 'instructional') != -1) && (this.productsPurchased.indexOf(this.allLevelsID) == -1)){
      console.log('if user purchased turns');
       //show green check on purchase card 
       this.showCheckLevels = true
       //hide buy button 
       this.showBuyBtnLevels = false
    }
    //if dont own level 
    else{
      this.getLevelData()
   // this.checkOwnStatus()
    }
  }
  else{
    this.getLevelData()
  }
  }

  getLevelData(){
    console.log('if user hasnt purhcased turns');
   //unhide buttons 
   this.showBuyBtnLevels = true
   console.log('if level buy btn', this.showBuyBtnLevels);
   console.log(this.instructionalArray, 'this.instructionalArray');
   //get price of level they picked to show on buy button 
      for (let index = 0; index < this.instructionalArray.length; index++) {
        console.log( this.instructionalArray.length, 'dana instructionalID.length');
        console.log(this.instructionalArray[0], 'dana instructARray at index 0');
          if( this.instructionalArray[index].prodID == 'com.movesofficial.' + (this.levelPicked.replace(/\s/g, "")).replace(/-/g, "").toLowerCase() + 'instructional'){
            this.instructionalID =  this.instructionalArray[index].prodID
            this.instructionalPrice =  this.instructionalArray[index].price
            this.instructionalOwnedStatus =  this.instructionalArray[index].owned
            console.log(this.instructionalID , this.instructionalPrice, 'dana this.instructionalID selected');
          }
      }
    console.log('dana in set up function');
    console.log('dana in structional owned status', this.instructionalOwnedStatus, this.allLevelsOwnedStatus);
  }


  buyItem(value: any) {
    console.log('dana in buy item funciton');
    console.log(this.OFFICIAL_PROD_SELECTED, 'this.OFFICIAL_PROD_SELECTED dana in buy tiem');
      this.modalCtrl.dismiss({
        'dismissed': true
      });
        console.log('dana #1 unlock page');
        //if purchase whole app
        if(value == "allLevels"){
          this.OFFICIAL_PROD_SELECTED = 'com.movesofficial.alllevels'
          console.log(this.OFFICIAL_PROD_SELECTED, 'this.OFFICIAL_PROD_SELECTED dana in buy all levels');
          console.log('dana in all levels if statement on unlock page #23');
          this.store.order(this.OFFICIAL_PROD_SELECTED).then((p) => {
            this.store.when(this.OFFICIAL_PROD_SELECTED)
            .approved((p: IAPProduct) => {
              console.log('dana item purchased approved #1');
              this.storeItem()
              this.ref.detectChanges();
              return p.verify();
            })
            .verified((p: IAPProduct) => p.finish());
          })
        }
        //if purhcase turns
        if(value == "turns"){
          console.log('dana in buy item funciton in turns');
          this.OFFICIAL_PROD_SELECTED = 'com.movesofficial.turnchecklist'
          this.store.order(this.OFFICIAL_PROD_SELECTED).then((p) => {
            this.store.when(this.OFFICIAL_PROD_SELECTED)
            .approved((p: IAPProduct) => {
              console.log('dana item purchased approved #1');
              this.storeItem()
              this.ref.detectChanges();
              return p.verify();
            })
            .verified((p: IAPProduct) => p.finish());
          })
        }

        //if purchase instructional levels
        if(value == "levels"){ 
          this.OFFICIAL_PROD_SELECTED = 'com.movesofficial.' + (this.levelPicked.replace(/\s/g, "")).replace(/-/g, "").toLowerCase() + 'instructional'
          console.log(this.OFFICIAL_PROD_SELECTED, 'this.OFFICIAL_PROD_SELECTED dana in buy all levels');
          console.log('dana in individualPurchase if statement on unlock page #24');
          this.store.order(this.OFFICIAL_PROD_SELECTED).then((p) => {
            this.store.when(this.OFFICIAL_PROD_SELECTED)
            .approved((p: IAPProduct) => {
              console.log('dana item purchased approved #1');
              this.storeItem()
              this.ref.detectChanges();
              return p.verify();
            })
            .verified((p: IAPProduct) => p.finish());
          })
        }
  }

  async restoreAlert(value: any) {
    const alert = await this.alertController.create({
      header: 'Already purchased?',
      mode: 'ios',
      message: '<p class="alertText">If you already purchased this item, but it is not available to you, click restore to restore it to your device.</p>',
      buttons: [{
          text: 'Cancel'
        },
        {
          text: 'Restore',
          handler: () => {
            this.restore(value)
          }
        }
      ]
    });
    await alert.present();
  }

  async promoAlert(value: any){
    if (this.plt.is('ios')) {
    const alert = await this.alertController.create({
      header: 'Have a promo code?',
      mode: 'ios',
      message: 
      '<p>1. Open App Store app.</p>'+
      '<p>2. Click top right Circle "account" Icon.</p>'+
      '<p>3. Click "Redeem Gift Card or Code".</p>'+
      '<p>4. Click "Enter Code Manually" and type code.</p>'+
      '<p>5. Come back to MITF app. Click "Activate Code" below to get access to the promo code content.</p>',
      // '<p class="alertText">Open your App Store app. Click on the top right Circle "account" Icon. Click "Redeem Gift Card or Code". Click "Enter Code Manually" and type code. Come back to MITF app. Click "Activate Code" below to get access to the promo code content.</p>',
      buttons: [{
          text: 'Cancel'
        },
        {
          text: 'Activate Code',
          handler: () => {
            this.restore(value)
          }
        }
      ]
    });
    await alert.present();
  }
  if (this.plt.is('android')) {
    const alert = await this.alertController.create({
      header: 'Have a promo code?',
      mode: 'ios',
      message: 
      '<p>1. Open Google Play app.</p>'+
      '<p>2. Click top left menu icon.</p>'+
      '<p>3. Click "Redeem" and type code.</p>'+
      '<p>4. Come back to MITF app. Click "Activate Code" below to get access to the promo code content.</p>',
      // '<p class="alertText">Open your App Store app. Click on the top right Circle "account" Icon. Click "Redeem Gift Card or Code". Click "Enter Code Manually" and type code. Come back to MITF app. Click "Activate Code" below to get access to the promo code content.</p>',
      buttons: [{
          text: 'Cancel'
        },
        {
          text: 'Activate Code',
          handler: () => {
            this.restore(value)
          }
        }
      ]
    });
    await alert.present();
  }
  }

    //this is basically for user with promo code...the code on the other pages makes the restore button basically useless
    restore(value) {
      console.log(value, 'dana restore type value');
      console.log(this.OFFICIAL_PROD_SELECTED, 'this.OFFICIAL_PROD_SELECTED dana in restore tiem');
      this.modalCtrl.dismiss({
        'dismissed': true
      });
      console.log('dana in restore function');
      if(value == 'allLevels'){
        this.OFFICIAL_PROD_SELECTED = 'com.movesofficial.alllevels'
        this.getStoreData()
      }
      if(value == 'levels'){
        this.OFFICIAL_PROD_SELECTED = 'com.movesofficial.' + (this.levelPicked.replace(/\s/g, "")).replace(/-/g, "").toLowerCase() + 'instructional'
        this.getStoreData()
      }
      if(value == 'turns'){
        this.OFFICIAL_PROD_SELECTED = 'com.movesofficial.turnchecklist'
        this.getStoreData()
      }
    }

    getStoreData(){
      this.store.when(this.OFFICIAL_PROD_SELECTED)
        .approved((p: IAPProduct) => {
          // Handle the product deliverable
          console.log(JSON.stringify(p), 'dana individual purchase??');
          this.ref.detectChanges();
          return p.verify();
        })
        .verified((p: IAPProduct) => p.finish())
  
        this.store.when(this.OFFICIAL_PROD_SELECTED).owned((p: IAPProduct) => {
          console.log(JSON.stringify(p), 'dana Im not sure what is going on 2');//this worked!!!!
          if (p.owned){
         //then show them product and store it to local storage
         this.storeItem()
        }
        else{
          alert('No previous purchase or promo code found')
        }
        });
        this.store.refresh();
    }

    storeItem(){
      console.log(this.OFFICIAL_PROD_SELECTED, 'this.OFFICIAL_PROD_SELECTED dana in store tiem');
      console.log('dana in store products --loop 2');
      //get items already purchased 
        this.productsPurchased = JSON.parse(localStorage.getItem('productsPurchased'))
        console.log(this.productsPurchased, 'dana products purchased parsed store item');
          if ((this.productsPurchased != undefined) || (this.productsPurchased != null)) {  
            //if user has purchased an item and it has not been stored yet 
            if ((this.productsPurchased.indexOf(this.OFFICIAL_PROD_SELECTED) == -1)) {
              console.log('dana item has not yet been stored and is about to be stored');
              console.log('dana not undefined --loop 3');
              this.productsPurchased.push(this.OFFICIAL_PROD_SELECTED);
              localStorage.setItem('productsPurchased', JSON.stringify(this.productsPurchased));
              this.showPurchasedCheckMark()
              //once set to store item then reload page 
             // this.authService.boughtAnItem()
            } 
          }
          if ((this.productsPurchased == undefined) || (this.productsPurchased == null)) {  
            console.log('dana in undefined');
            this.productsPurchased = []
            this.productsPurchased.push(this.OFFICIAL_PROD_SELECTED);
            localStorage.setItem('productsPurchased', JSON.stringify(this.productsPurchased));
            this.showPurchasedCheckMark()
            //once set to store item then reload page 
           // this.authService.boughtAnItem()
          }
    }

    showPurchasedCheckMark(){
      console.log('dana in showPurchasedCheckMark');
          //check to see if user has purcahsed all levels
          this.productsPurchased = JSON.parse(localStorage.getItem('productsPurchased'))
          console.log(this.productsPurchased, 'dana products purchased showpurcahsed checkmark function');
          console.log(this.productsPurchased, 'dana products purchased parsed showpurcahsed');
          //if user has purchased stuff before 
          if ((this.productsPurchased != null)) {
            console.log('dana products array does not equal null');
            //if user has purchased entire app 
            console.log(this.allLevelsID, 'dana products purchased allLevelsID');
            if ((this.productsPurchased.indexOf('com.movesofficial.alllevels') != -1)) {
              console.log('dana if user purchased entire app');
              //only show purchase card 
              this.showCardTurns = false
              this.showCardLevels = false
              //show green check on purchase card 
              this.showCheckEntireApp = true
              //hide buy button 
              this.showBuyBtnEntireApp = false
            } 
            console.log(this.turnID, 'dana products purchased turnID');
            //if turns is stored in purchased array and all levels isn't
            if((this.productsPurchased.indexOf('com.movesofficial.turnchecklist') != -1) && (this.productsPurchased.indexOf('com.movesofficial.alllevels') == -1)){
              console.log('dana if user purchased turns');
               //show green check on purchase card 
               this.showCheckTurns = true
               //hide buy button 
               this.showBuyBtnTurns = false
    
            }
            
            //user must select a level first..if they havent selected a level then dont show buy buttons or check mark
            if(this.levelPicked != null){
               //if user has purchased levels
            if(
              ((this.productsPurchased.indexOf('com.movesofficial.prepreliminaryinstructional') != -1) || 
              (this.productsPurchased.indexOf('com.movesofficial.preliminaryinstructional') != -1) || 
              (this.productsPurchased.indexOf('com.movesofficial.prejuvenileinstructional') != -1) || 
              (this.productsPurchased.indexOf('com.movesofficial.juvenileinstructional') != -1) || 
              (this.productsPurchased.indexOf('com.movesofficial.intermediateinstructional') != -1) || 
              (this.productsPurchased.indexOf('com.movesofficial.noviceinstructional') != -1) || 
              (this.productsPurchased.indexOf('com.movesofficial.juniorinstructional') != -1) || 
              (this.productsPurchased.indexOf('com.movesofficial.seniorinstructional') != -1) || 
              (this.productsPurchased.indexOf('com.movesofficial.adultprebronzeinstructional') != -1) || 
              (this.productsPurchased.indexOf('com.movesofficial.adultbronzeinstructional') != -1) || 
              (this.productsPurchased.indexOf('com.movesofficial.adultsilverinstructional') != -1) || 
              (this.productsPurchased.indexOf('com.movesofficial.adultgoldinstructional') != -1)) && 
              (this.productsPurchased.indexOf('com.movesofficial.alllevels') == -1))
              {
              console.log('dana if user purchased a level');
               //show green check on purchase card 
               this.showCheckLevels = true
               //hide buy button 
               this.showBuyBtnLevels = false
            }
            }
        }
        else{
          console.log('dana this product purchased is null');
          
        }
    }

}
