import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TurnsPage } from './turns.page';

describe('TurnsPage', () => {
  let component: TurnsPage;
  let fixture: ComponentFixture<TurnsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TurnsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TurnsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
