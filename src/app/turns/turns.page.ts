import { Component, OnInit } from '@angular/core';
import { AngularFirestore} from '@angular/fire/firestore';
import { ActionSheetController, AlertController, ModalController, ToastController, Platform} from '@ionic/angular'
import { TurnSheetComponent } from '../components/turn-sheet/turn-sheet.component';
import { AnalyticsService } from '../services/analytics/analytics.service';
import { UnlockComponent } from '../components/unlock/unlock.component';
import * as confetti from 'canvas-confetti';
import { IAPProduct } from '@ionic-native/in-app-purchase-2/ngx';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-turns',
  templateUrl: './turns.page.html',
  styleUrls: ['./turns.page.scss'],
})
export class TurnsPage implements OnInit {
  turns = [
    { name: "Forward Outside 3-turn", isChecked: false, id: '3RB1aJlZY5OG0iui8Xbz' },
    { name: "Forward Inside 3-turn", isChecked: false, id: '4tRjpPLDNfk9oCmiDEU2' },
    { name: "Backward Outside 3-turn", isChecked: false, id: 'GOP30CP5E6yBvHEzjDH5' },
    { name: "Backward Inside 3-turn", isChecked: false, id: 'Gwc8xvvWZqwG7Y0R8xwf' },

    { name: "Forward Outside Mohawk", isChecked: false, id: 'JkBMRCSds6SVDqsNKbT0' },
    { name: "Forward Inside Mohawk", isChecked: false, id: 'LNPkmqSCwV248cO5u5h8' },
    { name: "Backward Outside Mohawk", isChecked: false, id: 'LO3hjDXKnQ1jRxGT6Bya5N' },
    { name: "Backward Inside Mohawk", isChecked: false, id: 'LPwlyWf5WJsb0u85b2Lbg0' },

    { name: "Forward Outside Bracket", isChecked: false, id: 'NMPl54mAWGqMV7KydsNn' },
    { name: "Forward Inside Bracket", isChecked: false, id: 'PcCAXcJqPNW06ICpxIld' },
    { name: "Backward Outside Bracket", isChecked: false, id: 'V4LrBxhfdjLLWCESMLSj' },
    { name: "Backward Inside Bracket", isChecked: false, id: 'Xvlkc1JHPpMZYeW28BbU' },

    { name: "Forward Outside Counter", isChecked: false, id: 'anL9AC3LoqQmdYuirDaS' },
    { name: "Forward Inside Counter", isChecked: false, id: 'f344F2604teSNjw4tL1v' },
    { name: "Backward Outside Counter", isChecked: false, id: 'gNIQ6fc0J3fXVoGVJtJD' },
    { name: "Backward Inside Counter", isChecked: false, id: 'hpmUNUda9kI2NFUm7Qck' },

    { name: "Forward Outside Rocker", isChecked: false, id: 'nBqctmTliJSyluhTeVrT' },
    { name: "Forward Inside Rocker", isChecked: false, id: 'njqkMFoIviQZZ6HdaFuM' },
    { name: "Backward Outside Rocker", isChecked: false, id: 'rC5MIRNlT2ndfKmqQ50g' },
    { name: "Backward Inside Rocker", isChecked: false, id: 's6XAjZcONrAlxvjaN5vK' },

    { name: "Forward Outside Twizzle", isChecked: false, id: 'x3hs1BhNizhm4IDcUgfe9' },
    { name: "Forward Inside Twizzle", isChecked: false, id: 'x448g7wvbEeMnxzqG9hx3' },
    { name: "Backward Outside Twizzle", isChecked: false, id: 'xCvYOpQhiH5mQJYr3GwIH' },
    { name: "Backward Inside Twizzle", isChecked: false, id: 'xJQHd4k9SMuZkg2LbvpXJ' },

    { name: "Forward Outside Loop", isChecked: false, id: 'xMW2bjcqyDfql7cuwSUzH' },
    { name: "Forward Inside Loop", isChecked: false, id: 'xZWDfAF5m06TBgaEk4zrD' },
    { name: "Backward Outside Loop", isChecked: false, id: 'xdBcLqNN5mY2gX5tQrl46' },
    { name: "Backward Inside Loop", isChecked: false, id: 'xl2l4pOJmkc3M5I2GwDs' },

    { name: "Forward Inside Choctaw", isChecked: false, id: 'xl4r8GncrU5tnUFvXIt97VZ' },
    { name: "Backward Outside Choctaw", isChecked: false, id: 'xuJzjrpUtJcSqak9ugZn4' },
    { name: "Backward Inside Choctaw", isChecked: false, id: 'xvB6iOH0Bvg7lAF6yW47M' },
    ]
  productsPurchased = []
  savedTurns = []
  turnPreview
  OFFICIAL_PROD_SELECTED
  price
  products: IAPProduct[] = [];
  //product variable names
  allLevelsID
  allLevelsPrice
  allLevelsOwnedStatus
  allLevelsArray
  turnID
  turnPrice
  turnOwnedStatus
  turnArray

  constructor(
    public af: AngularFirestore,
    public actionSheetController: ActionSheetController,
    public alertController: AlertController,
    public modalController: ModalController,
    public toastController: ToastController,
    private authService: AuthService,
  ) {
    this.authService.passAllLevels$.subscribe((allLevelsID) => {
      console.log('dana in auth service on patterns page');
      this.allLevelsArray = JSON.parse(allLevelsID)
      this.allLevelsID = this.allLevelsArray[0].prodID
      this.allLevelsPrice = this.allLevelsArray[0].price
      this.allLevelsOwnedStatus = this.allLevelsArray[0].owned
      console.log(this.allLevelsID ,this.allLevelsPrice, this.allLevelsOwnedStatus, 'dana this.allLevelsID');
     })

     this.authService.passTurnChecklist$.subscribe((turnChecklistID) => {
      console.log('dana in auth service on patterns page');
      this.turnArray = JSON.parse(turnChecklistID)
      this.turnID = this.turnArray[0].prodID
      this.OFFICIAL_PROD_SELECTED = "com.movesofficial.turnchecklist"
      this.turnPrice = this.turnArray[0].price
      this.turnOwnedStatus = this.turnArray[0].owned
      console.log(this.turnID ,this.turnPrice, this.turnOwnedStatus, 'dana this.turnChecklistID');
     })
   console.log = function(){};
      this.af.collection("Links").doc('previews').ref.get().then((doc) =>{
        this.turnPreview = doc.data()['turns'];
      });
      this.savedTurns = JSON.parse(localStorage.getItem('savedTurns'))
       if(this.savedTurns != null){
           //if(this.savedQuotes != undefined){
             if(this.savedTurns.length > 0){
                       //loop through both arrays and see if there are any quotes in common 
                       for (let index = 0; index < this.turns.length; index++) {
                         //quoteID = quote unique ids
                         var turnName = this.turns[index].name;
                         //loop through users' savedQuotes
                         for (let num = 0; num < this.savedTurns.length; num++) {
                           if (turnName == this.savedTurns[num]) {
                             //see if quoteID exists in savedQuotes array 
                             const almostThere = this.turns.indexOf(this.turns[index]);
                             //if a  quoteID is in savedQuotes array 
                             if (almostThere >  -1) {
                               //then splice that index out of the array 
                               this.turns[index].isChecked = true;                              
                             }
                           }
                         }
                       }
             }
       }
   }

  ngOnInit() {
     //call price and owned variables 
     this.authService.passAllLevels()
     this.authService.passTurnChecklist()    
}

  isChecked(event){
    //if turn is checked
      if (event.detail.checked) {
        this.congratsAlert(event)
        this.savedTurns = JSON.parse(localStorage.getItem('savedTurns'));
       //have saved some turns
        if(this.savedTurns != null){
          console.log('if is checked');
          
        this.savedTurns.push(event.detail.value);
        localStorage.setItem('savedTurns', JSON.stringify(this.savedTurns));
        }
        //first saved turn
        else{
          console.log('else is checked');
          this.savedTurns = []
          this.savedTurns.push(event.detail.value);
          localStorage.setItem('savedTurns', JSON.stringify(this.savedTurns));
        }
        }
        //if turn is unchecked
        else{
          console.log('if is checked else twice');
          this.savedTurns = JSON.parse(localStorage.getItem('savedTurns'));
                const index = this.savedTurns.indexOf(event.detail.value);
                if (index > -1) {
                  this.savedTurns.splice(index, 1);
                  localStorage.setItem('savedTurns', JSON.stringify(this.savedTurns));
                }
        }
    } 

    async congratsAlert(event) {
      var canvas = document.getElementById('my-canvas');

      var myConfetti = confetti.create(canvas, {
        resize: true,
        useWorker: true
      });

      myConfetti({
        particleCount: 200,
        spread: 160
      });
      const toast = await this.toastController.create({
        mode: 'ios',
        position: 'middle',
        cssClass: 'toast-class',
        message: 'Congrats on mastering your <br>' + event.detail.value + '!',
        duration: 2000
      });
      toast.present();
    }

    checkPurchaseStatus(item){
      console.log('dana in check user purchase function');
       this.productsPurchased = JSON.parse(localStorage.getItem('productsPurchased'))
        console.log(this.productsPurchased, 'dana products purchased parsed');
        //if user has purchased stuff before 
        if ((this.productsPurchased != null) && (this.productsPurchased != undefined)) {
          //if user has purchased what they click on then give them access 
          if ((this.productsPurchased.indexOf('com.movesofficial.turnchecklist') != -1) || (this.productsPurchased.indexOf('com.movesofficial.alllevels') != -1)) {
           this.nextPage(item)
          } 
          //if user hasn't purchased what they click on prepare to show them the store page 
          else {
              this.checkOwnStatus(item);
          }
        }
        //if user has never purchased stuff before 
        else{
              this.checkOwnStatus(item);
        }
    }

    checkOwnStatus(item){
      console.log('dana in one more thing value function');
      //alert('in turns check own sttaus....dana* then it went here')
      if((this.turnOwnedStatus == true) || (this.allLevelsOwnedStatus == true)){
        console.log('dana in ready true');
        //and you will save to local storage here****
        this.setVariableToStore()
        this.nextPage(item)
      }
      if((this.turnOwnedStatus == false) && (this.allLevelsOwnedStatus == false)){
        console.log('dana in ready false');
        this.showUnlockPage(item)
      }
    }

    setVariableToStore(){
      //alert('in turns store')
      console.log('dana in store products');
         this.productsPurchased = JSON.parse(localStorage.getItem('productsPurchased'))
         //if this.allLevelsOwnedStatus == true store all levels 
         if(this.allLevelsOwnedStatus == true){
          //alert('in turns store 1')
           this.OFFICIAL_PROD_SELECTED = this.allLevelsID
           this.storeItem()
         }
         //if this.instructionalOwnedStatus == true and the above is not true then store the instructional content
          if((this.turnOwnedStatus == true) && (this.allLevelsOwnedStatus == false)){
           // alert('in turns store 2')
           this.storeItem()
          }
       }

        storeItem(){
         // alert('in turns store!')
          if ((this.productsPurchased != undefined) || (this.productsPurchased != null)) {  
            if ((this.productsPurchased.indexOf(this.OFFICIAL_PROD_SELECTED) == -1)) {
            console.log('dana not undefined');
            //alert('in turns store 1!')
            this.productsPurchased.push(this.OFFICIAL_PROD_SELECTED);
            localStorage.setItem('productsPurchased', JSON.stringify(this.productsPurchased));
            }
          }
          if ((this.productsPurchased == undefined) || (this.productsPurchased == null)) {  
            console.log('dana in undefined');
            //alert('in turns store 2!')
            this.productsPurchased = []
            this.productsPurchased.push(this.OFFICIAL_PROD_SELECTED);
            localStorage.setItem('productsPurchased', JSON.stringify(this.productsPurchased));
          }
        }

  async showUnlockPage(item){
    console.log(this.turnID, 'dana #7 this.turnID');
    console.log(this.turnPrice, 'dana #7 this.turnPrice');
    console.log(this.allLevelsPrice, 'dana #7 this.allLevelsPrice');
    const modal = await this.modalController.create({
      component: UnlockComponent,
      cssClass: "modal-fullscreen",
      componentProps: {
        unlockBullets: [
          'Slow motion videos of all turns with strap', 
          'Breakdown of each turn technique',
          'Exercises for each turn',
          'On ice drawings of turn tracings', 
          'Definition of each turn',
          'List of patterns that include each turn' 
        ],
        unlockPreview: this.turnPreview,
        productDisplayName: 'Instructional content for all MITF turns',
        OFFICIAL_PROD_SELECTED: this.turnID,
        productType: 'checklist',
        price: this.turnPrice,
        id: item.id,
        turnName: item.name,
        allLevelsPrice: this.allLevelsPrice,
      }
    });
    return await modal.present();
  }

  async nextPage(item){
      const modal = await this.modalController.create({
        component: TurnSheetComponent,
        cssClass: "modal-fullscreen",
        componentProps: {
        id: item.id,
        turnName: item.name
        }
      });
      return await modal.present();
  }

}
