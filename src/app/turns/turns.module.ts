import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TurnsPageRoutingModule } from './turns-routing.module';

import { TurnsPage } from './turns.page';
import { ComponentsModule } from '../components/components.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TurnsPageRoutingModule,
    ComponentsModule
  ],
  declarations: [TurnsPage]
})
export class TurnsPageModule {}
