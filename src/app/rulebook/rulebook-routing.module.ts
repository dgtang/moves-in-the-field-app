import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RulebookPage } from './rulebook.page';

const routes: Routes = [
  {
    path: '',
    component: RulebookPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RulebookPageRoutingModule {}
