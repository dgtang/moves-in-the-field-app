import { Component, ChangeDetectorRef, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFirestore} from '@angular/fire/firestore';
import { LoadingController, ModalController} from '@ionic/angular'
import { QuizComponent } from '../components/quiz/quiz.component';

@Component({
  selector: 'app-rulebook',
  templateUrl: './rulebook.page.html',
  styleUrls: ['./rulebook.page.scss'],
})
export class RulebookPage implements OnInit {
  rulebookURL
  rulebookPages = []
  category = "standard"

  constructor(
    public af: AngularFirestore,
    public loadingController: LoadingController,
    private router: Router,
    public modalCtrl: ModalController,
    private cf: ChangeDetectorRef,
  ) { 
  
  }

  segmentChanged(ev: any) {
    this.cf.detectChanges();
    if(ev.detail.value == "standard"){
      this.category = "standard"
    }
    if(ev.detail.value == "adult"){
      this.category = "adult"
    }
    if(ev.detail.value == "adaptive"){
      this.category = "adaptive"
    }
  }

  async openQuiz(value :any) {
    const modal = await this.modalCtrl.create({
      component: QuizComponent,
      cssClass: "modal-fullscreen",
      componentProps: {
        testTitle: value
      }
    });
    return await modal.present();
}

  ngOnInit() {
  }

  // Show the loader for infinite time
  async showLoader() {

    const loading = await this.loadingController.create({
      message: '',
   });
   return loading.present();

  }

}
