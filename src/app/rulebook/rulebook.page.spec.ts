import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RulebookPage } from './rulebook.page';

describe('RulebookPage', () => {
  let component: RulebookPage;
  let fixture: ComponentFixture<RulebookPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RulebookPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RulebookPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
