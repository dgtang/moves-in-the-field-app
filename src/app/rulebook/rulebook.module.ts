import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RulebookPageRoutingModule } from './rulebook-routing.module';

import { RulebookPage } from './rulebook.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RulebookPageRoutingModule
  ],
  declarations: [RulebookPage]
})
export class RulebookPageModule {}
