import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

// angular fire module 
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFirestoreModule } from '@angular/fire/firestore';

//providers
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { InAppPurchase2 } from '@ionic-native/in-app-purchase-2/ngx';

//components
import { ComponentsModule } from '../app/components/components.module';
import { QuizComponent } from '../app/components/quiz/quiz.component';
import { TurnSheetComponent } from '../app/components/turn-sheet/turn-sheet.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';


export const firebaseConfig = {
  apiKey: "AIzaSyCVDorS73ieacxzp5ZGnxIFmFbH2WBzKe0",
  authDomain: "movesofficial2.firebaseapp.com",
  databaseURL: "https://movesofficial2.firebaseio.com",
  projectId: "movesofficial2",
  storageBucket: "movesofficial2.appspot.com",
  messagingSenderId: "877622231743",
  appId: "1:877622231743:web:9373643202aab39f53bc0a",
  measurementId: "G-SRHXBYQQFC"
};

@NgModule({
  declarations: [AppComponent],
  entryComponents: [
    QuizComponent,
    TurnSheetComponent,
  ],
  imports: [
    BrowserModule, 
    IonicModule.forRoot(), 
    AppRoutingModule,
    HttpClientModule,
    AngularFireModule.initializeApp(firebaseConfig),
    //this allows user to use the app off line
    AngularFirestoreModule.enablePersistence(),
    AngularFireDatabaseModule,
    AngularFirestoreModule,
    ComponentsModule,
    BrowserAnimationsModule,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    InAppPurchase2,
    HttpClient,
    InAppBrowser,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}


