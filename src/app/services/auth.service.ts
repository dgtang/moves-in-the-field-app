import { Injectable } from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import { AlertController, ModalController} from '@ionic/angular';
import { Observable, Subject } from 'rxjs';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  resetEmailAccount;
  changeStuffShown$: Observable<any>;
  private changeStuffShownSubject = new Subject<any>();
  itemsFromStore$: Observable<any>;
  private itemsFromStoreSubject = new Subject<any>();
  passAllLevels$: Observable<any>;
  private passAllLevelsSubject = new Subject<any>();
  passInstructionalInfo$: Observable<any>;
  private passInstructionalInfoSubject = new Subject<any>();
  passQuizInfo$: Observable<any>;
  private passQuizInfoSubject = new Subject<any>();
  passTurnChecklist$: Observable<any>;
  private passTurnChecklistSubject = new Subject<any>();
  // boughtAnItem$: Observable<any>;
  // private boughtAnItemSubject = new Subject<any>();
  
  technique
  techniqueTitle
  slowVideo: SafeResourceUrl;
  watchFor = []
  blade = []
  variation = []
  moreTechniqueInfo = []
  passingArray = []
  storeItemsArray = []
  danaTest
  allLevelsID
  turnChecklistID
  instructionalID

  constructor(
    public af: AngularFirestore,
    public alertController: AlertController,
    public modalCtrl: ModalController,
    private domSanitizer: DomSanitizer
  ) { 
    this.changeStuffShown$ = this.changeStuffShownSubject.asObservable();
    this.itemsFromStore$ = this.itemsFromStoreSubject.asObservable();
    this.passAllLevels$ = this.passAllLevelsSubject.asObservable();
    this.passInstructionalInfo$ = this.passInstructionalInfoSubject.asObservable();
    this.passTurnChecklist$ = this.passTurnChecklistSubject.asObservable();
   // this.boughtAnItem$ = this.boughtAnItemSubject.asObservable();
  }
  
  testing(buyStatus){        
    //this will call check user testing on pattern details again 
    this.changeStuffShownSubject.next(buyStatus);
  }

  changeStuffShown(
    techniques, 
    techniqueTitles, 
    slowVideos,
    watchFors,
    blades,
    variations,
    moreTechniqueInfos,
    arrayIndex){
    //make them equal 
    this.technique = techniques[arrayIndex]
    this.techniqueTitle = techniqueTitles[arrayIndex]
    this.slowVideo = this.domSanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/'+ slowVideos[arrayIndex] +'?rel=0&loop=1&modestbranding=1&showinfo=0&autoplay=0')
    this.watchFor = watchFors[arrayIndex]
    this.blade = blades[arrayIndex]
  
    if(variations != undefined){
      this.variation = variations[arrayIndex]
    }
    if(moreTechniqueInfos != undefined){
    this.moreTechniqueInfo = moreTechniqueInfos[arrayIndex]
    }

    this.passingArray = []
    this.passingArray.push({
      technique: this.technique, 
      techniqueTitle: this.techniqueTitle, 
      slowVideo: this.slowVideo,
      watchFor: this.watchFor,
      blade: this.blade,
      variation: this.variation,
      moreTechniqueInfo: this.moreTechniqueInfo,
    })
    console.log(this.passingArray, 'this.passingArray');
    
    this.changeStuffShownSubject.next(this.passingArray);
}

// boughtAnItem(){

// }

itemsFromStore(allLevelsID, turnChecklistID, instructionalID){
  console.log(allLevelsID, 'dana in service allLevelsID');
  console.log(turnChecklistID, 'dana in service turnChecklistID');
  console.log(instructionalID, 'dana in service instructionalID');
  this.allLevelsID = allLevelsID
  this.turnChecklistID = turnChecklistID
  this.instructionalID = instructionalID
}

//entire app
passAllLevels(){
  console.log('dana in pass data funciton in auth');
  console.log(this.allLevelsID, 'dana in service allLevelsID in pass levels function');
  this.passAllLevelsSubject.next(this.allLevelsID);
}

passInstructionalInfo(){
  this.passInstructionalInfoSubject.next(this.instructionalID)
}

passTurnChecklist(){
  this.passTurnChecklistSubject.next(this.turnChecklistID)
}

}

//import the service
//import { AuthService } from '../services/auth.service';
//private authService: AuthService,

//push content
//this.authService.changeStuffShown(this.item)

//call in ngoninit
//this.authService.changeStuffShown();

//grab in constructor
// this.authService.changeStuffShown$.subscribe((userType) => {
//   this.userType = userType
//   this.switchToHomePage = true;
//   this.announcement()
//  })



