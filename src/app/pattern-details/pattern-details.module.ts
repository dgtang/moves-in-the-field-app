import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PatternDetailsPageRoutingModule } from './pattern-details-routing.module';

import { PatternDetailsPage } from './pattern-details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PatternDetailsPageRoutingModule
  ],
  declarations: [PatternDetailsPage]
})
export class PatternDetailsPageModule {}
