import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PatternDetailsPage } from './pattern-details.page';

const routes: Routes = [
  {
    path: '',
    component: PatternDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PatternDetailsPageRoutingModule {}
