import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlertController, ModalController, LoadingController, Platform} from '@ionic/angular';
import { AngularFirestore} from '@angular/fire/firestore';
import { ErrorsComponent } from '../components/errors/errors.component';
import { UnlockComponent } from '../components/unlock/unlock.component';
import { ExercisesComponent } from '../components/exercises/exercises.component';
import { InstructionComponent } from '../components/instruction/instruction.component';
import { IntroduceComponent } from '../components/introduce/introduce.component';
import { PlacementComponent } from '../components/placement/placement.component';
import { ReadyComponent } from '../components/ready/ready.component';
import { IAPProduct } from '@ionic-native/in-app-purchase-2/ngx';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { AuthService } from '../services/auth.service';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Location } from "@angular/common";
import { Browser } from '@capacitor/browser';

@Component({
  selector: 'app-pattern-details',
  templateUrl: './pattern-details.page.html',
  styleUrls: ['./pattern-details.page.scss'],
})
export class PatternDetailsPage implements OnInit {
  item
  focus
  testName;
  isLoading = false;
  OFFICIAL_PROD_SELECTED
  productsPurchased = []
  comingSoon;
  patternInfo = []
  collectionName
  id
  title
  componentName
  price
  bulletsArray = []
  adultExpandedLevel = ''
  products: IAPProduct[] = [];
  isLoading2 = false;
  //product variable names
  allLevelsID
  allLevelsPrice
  allLevelsOwnedStatus
  allLevelsArray
  instructionalID
  instructionalPrice
  instructionalOwnedStatus
  instructionalArray
  coachVideoLoaded = false
  coachVideoLoadedValue = 0
  skater1VideoLoaded = false
  skater1VideoLoadedValue = 0
  skater2VideoLoaded = false
  skater2VideoLoadedValue = 0
  skater3VideoLoaded = false
  skater3VideoLoadedValue = 0
  isIpad = false

  constructor(
    public route: ActivatedRoute,
    public alertController: AlertController,
    public modalCtrl: ModalController,
    public loadingCtrl: LoadingController,
    public af: AngularFirestore,
    private domSanitizer: DomSanitizer,
    private authService: AuthService,
    private location: Location,
    public plt: Platform,  
    private iab: InAppBrowser,
  ) {       
    if((this.plt.is('ios')) && (this.plt.is('ipad'))){
      this.isIpad = true
    }
      this.authService.passAllLevels$.subscribe((allLevelsID) => {
        console.log('dana in auth service on patterns page');
        this.allLevelsArray = JSON.parse(allLevelsID)
        this.allLevelsID = this.allLevelsArray[0].prodID
        this.allLevelsPrice = this.allLevelsArray[0].price
        this.allLevelsOwnedStatus = this.allLevelsArray[0].owned
        console.log(this.allLevelsID ,this.allLevelsPrice, this.allLevelsOwnedStatus, 'dana this.allLevelsID');
       })

       this.authService.passInstructionalInfo$.subscribe((instructionalID) => {
        console.log('dana in auth service on patterns page 2!!');
        this.instructionalArray = JSON.parse(instructionalID)
        console.log(this.instructionalArray, 'dana this.instructionalArray');
        console.log(this.instructionalArray.length, 'dana this.instructionalArray.length');
       })
   console.log = function(){};

   this.showLoader().then(() => {
    this.route.queryParams.subscribe((res) => {
      this.af.collection(res.collectionName).doc(res.id).ref.get().then((doc) => {
        this.patternInfo.push({
          bullets: doc.data()['bullets'],
          title: doc.data()['title'],
          diagramURL: doc.data()['diagramURL'],
          focus: doc.data()['focus'],
          youtubeURL1: this.domSanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/'+ doc.data()['youtubeURL1'] +'?rel=0&loop=1&modestbranding=1&showinfo=0&autoplay=0'),
          youtubeURL2: this.domSanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/'+ doc.data()['youtubeURL2'] +'?rel=0&loop=1&modestbranding=1&autoplay=0&showinfo=0'),
          youtubeURL2Code: doc.data()['youtubeURL2'],
          videoDesc1: doc.data()['videoDesc1'],
          videoDesc2: doc.data()['videoDesc2'],
          desc: doc.data()['desc'].split('.'),
          youtubeURLAdult: this.domSanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/'+ doc.data()['youtubeURLAdult'] +'?rel=0&loop=1&modestbranding=1&autoplay=0&showinfo=0'),
          youtubeURLAdultCode: doc.data()['youtubeURLAdult'],
        });
        this.bulletsArray = doc.data()['bullets']
        this.collectionName = res.collectionName;
        this.id = res.id;
      }).then(() => {
        //top
        if ((res.testName == 'Adult Intermediate') ||
        (res.testName == 'Adult Novice') ||
        (res.testName == 'Adult Junior') ||
        (res.testName == 'Adult Senior')) {
          this.adultExpandedLevel = res.testName
          this.testName = res.testName.split('Adult ')[1];
          this.pullData()
       
      } else {
        this.testName = res.testName
        this.pullData()
      }
      })
    });
  })
    }

   async viewDiagram(item){
    if (this.plt.is('ios')) {
      await Browser.open({ url: item.diagramURL });
    }
    if (this.plt.is('android')) {
      this.iab.create(item.diagramURL, '_blank'); 
    }
    }

    //video loading dots 
    coachIframeLoaded(){
      if(this.coachVideoLoadedValue == 0){
        this.coachVideoLoadedValue = this.coachVideoLoadedValue + 1
      }
      else{
        this.coachVideoLoaded = true
      }
    }

    skater1IframeLoaded(){
      if(this.skater1VideoLoadedValue == 0){
        this.skater1VideoLoadedValue = this.skater1VideoLoadedValue + 1
      }
      else{
        this.skater1VideoLoaded = true
      }
    }

    skater2IframeLoaded(){
      if(this.skater2VideoLoadedValue == 0){
        this.skater2VideoLoadedValue = this.skater2VideoLoadedValue + 1
      }
      else{
        this.skater2VideoLoaded = true
      }
    }

    skater3IframeLoaded(){
      if(this.skater3VideoLoadedValue == 0){
        this.skater3VideoLoadedValue = this.skater3VideoLoadedValue + 1
      }
      else{
        this.skater3VideoLoaded = true
      }
    }

    //end video loading dots 
    pullData(){
      this.OFFICIAL_PROD_SELECTED = 'com.movesofficial.' + (this.testName.replace(/\s/g, "")).replace(/-/g, "").toLowerCase() + 'instructional';
      console.log(this.OFFICIAL_PROD_SELECTED, 'this.OFFICIAL_PROD_SELECTED');
      console.log(this.testName, 'this.testName');
      this.af.collection("comingSoon").doc('kzCDM6cSpm9qXHjRM1Or').ref.get().then((doc) => {
        this.comingSoon = doc.data()['comingSoon'];
      })
      this.loadingCtrl.dismiss();
    }

    ngOnInit() {
      console.log('dana in ngoninit');
      
    //call price and owned variables 
    this.authService.passAllLevels()
    this.authService.passInstructionalInfo()
    }

    async showLoader() {
      const loading = await this.loadingCtrl.create({
        message: '',
      });
      return loading.present();
    }
    
    async openUnlockModal() {
      const modal = await this.modalCtrl.create({
        component: UnlockComponent,
        cssClass: "modal-fullscreen",
        componentProps: {
          title: this.testName,
        }
      });
      return await modal.present();
    }

    checkPurchaseStatus(value :any){
      console.log('dana in check user purchase function');
       this.productsPurchased = JSON.parse(localStorage.getItem('productsPurchased'))
        console.log(this.productsPurchased, 'dana products purchased parsed');
        console.log(this.OFFICIAL_PROD_SELECTED, 'dana this.OFFICIAL_PROD_SELECTED');
        //if user has purchased stuff before 
        if ((this.productsPurchased != null) && (this.productsPurchased != undefined)) {
          //if user has purchased what they click on then give them access 
          if ((this.productsPurchased.indexOf(this.OFFICIAL_PROD_SELECTED) != -1) || (this.productsPurchased.indexOf('com.movesofficial.alllevels') != -1)) {
            console.log('dana buy button false on test details page');
          this.nextPage(value)
          } 
          //if user hasn't purchased what they click on prepare to show them the store page 
          else {
              console.log('dana in platform ready1');            
              this.getIndividualPurchaseData(value);
          }
        }
        //if user has never purchased stuff before 
        else{
              console.log('dana in platform ready2');
              this.getIndividualPurchaseData(value);
        }
    }

    getIndividualPurchaseData(value) {
       for (let index = 0; index < this.instructionalArray.length; index++) {
          console.log( this.instructionalArray.length, 'dana instructionalID.length');
          console.log(this.instructionalArray[0], 'dana instructARray at index 0');
            if( this.instructionalArray[index].prodID == this.OFFICIAL_PROD_SELECTED){
              this.instructionalID =  this.instructionalArray[index].prodID
              this.instructionalPrice =  this.instructionalArray[index].price
              this.instructionalOwnedStatus =  this.instructionalArray[index].owned
              console.log(this.instructionalID , this.instructionalPrice, 'dana this.instructionalID selected');
            }
        }
      console.log('dana in set up function');
      console.log('dana in structional owned status', this.instructionalOwnedStatus, this.allLevelsOwnedStatus);
      this.checkOwnStatus(value)
    }

    checkOwnStatus(value){
      console.log('dana in one more thing value function');
      
      if((this.instructionalOwnedStatus == true) || (this.allLevelsOwnedStatus == true)){
        console.log('dana in ready true');
        //and you will save to local storage here****
        this.setVariableToStore()
        this.nextPage(value)
      }
      if((this.instructionalOwnedStatus == false) && (this.allLevelsOwnedStatus == false)){
        console.log('dana in ready false');
        this.showUnlockPage(value)
      }
    }

    setVariableToStore(){
         console.log('dana in store products');
            this.productsPurchased = JSON.parse(localStorage.getItem('productsPurchased'))
            //if this.allLevelsOwnedStatus == true store all levels 
            if(this.allLevelsOwnedStatus == true){
              this.OFFICIAL_PROD_SELECTED = this.allLevelsID
              this.storeItem()
            }
            //if this.instructionalOwnedStatus == true and the above is not true then store the instructional content
             if((this.instructionalOwnedStatus == true) && (this.allLevelsOwnedStatus == false)){
              this.OFFICIAL_PROD_SELECTED = this.instructionalID
              this.storeItem()
             }
          }

    storeItem(){
      if ((this.productsPurchased != undefined) || (this.productsPurchased != null)) {  
        if ((this.productsPurchased.indexOf(this.OFFICIAL_PROD_SELECTED) == -1)) {
        console.log('dana not undefined');
        this.productsPurchased.push(this.OFFICIAL_PROD_SELECTED);
        localStorage.setItem('productsPurchased', JSON.stringify(this.productsPurchased));
        this.dismiss2()
        }
      }
      if ((this.productsPurchased == undefined) || (this.productsPurchased == null)) {  
        console.log('dana in undefined');
        this.productsPurchased = []
        this.productsPurchased.push(this.OFFICIAL_PROD_SELECTED);
        localStorage.setItem('productsPurchased', JSON.stringify(this.productsPurchased));
        this.dismiss2()
      }
    }

    async showUnlockPage(value){
      console.log('in show unlock page dana');
      const modal = await this.modalCtrl.create({
        component: UnlockComponent,
        cssClass: "modal-fullscreen",
        componentProps: {
          unlockBullets: [
            'Technique and variations for all patterns',
            'Slow motion videos for all patterns',
            'Errors & corrections for all patterns', 
            'Exercises for each test expectation', 
            'How to introduce each pattern',
            'Pattern placement for all patterns',
            'How to know when ready to test',
          ],
          productDisplayName: this.testName + ' Instructional Content',
          OFFICIAL_PROD_SELECTED: this.OFFICIAL_PROD_SELECTED,
          productType: value,
          id: this.id,
          collectionName: this.collectionName,
          price: this.instructionalPrice,
          testName: this.testName,
          allLevelsPrice: this.allLevelsPrice
        }
      });
      return await modal.present();
    }

    async nextPage(value){
        if(value == 'exercise'){
          this.componentName = ExercisesComponent
          this.openInstructional()
        }
        if(value == 'error'){
          this.componentName = ErrorsComponent
          this.openInstructional()
        }
        if(value == 'detail'){
          this.componentName = InstructionComponent
          this.openInstructional()
        }
        if(value == 'introduce'){
          this.componentName = IntroduceComponent
          this.openInstructional()
        }
        if(value == 'placement'){
          this.componentName = PlacementComponent
          this.openInstructional()
        }
        if(value == 'ready'){
          this.componentName = ReadyComponent
          this.openInstructional()
        }
    }

    async openInstructional() {
      const modal = await this.modalCtrl.create({
        component: this.componentName,
        cssClass: "modal-fullscreen",
        componentProps: {
          id: this.id,
          collectionName: this.collectionName,
          bullets: this.bulletsArray,
        }
      });
      return await modal.present();
    }

    async present() {
      this.isLoading = true;
      return await this.loadingCtrl.create({
        message: '',
      }).then(a => {
        a.present().then(() => {
          if (!this.isLoading) {
            a.dismiss().then(() => console.log('abort presenting'));
          }
        });
      });
    }

    async dismiss() {
      this.isLoading = false;
      return await this.loadingCtrl.dismiss().then(() => console.log('dismissed'));
    }

    getFocusDefinition(item, i) {
      this.focus = item.focus[i];
      console.log(this.focus);
      
      this.displayFocusDefinition();
    }

    async displayFocusDefinition() {
      if (this.focus.includes('power')) {
        const alert = await this.alertController.create({
          header: 'Definition of Power',
          mode: 'ios',
          message: '<p>The creation and maintenance of speed and flow without visible effort. It is developed by a continuous rise and fall of the skating knee together with the pressure of the edge of the blade against the ice.' +
            ' The skater should demonstrate the ability to exert equal pressure against the surface of the ice on both the right and left foot.' +
            ' End products of power are (1) velocity, speed or pace; (2) flow across the ice; and (3) acceleration.</p>',
          buttons: ['Close']
        });
        await alert.present();
      }

      if (this.focus.includes('quickness')) {
        const alert = await this.alertController.create({
          header: 'Definition of Quickness',
          mode: 'ios',
          message: '<p>Refers to foot speed. It is precise, rapid and crisp execution of turns, changes of edge and transitions. Quickness does not refer to the overall pace at which the move is skated, although in some moves the foot speed will result in a brisk and continuous cadence.' +
            ' Refinements to acknowledge include quick movement that is quiet, fluid and continuous without disturbing the proper and erect carriage of the upper body or interrupting the established rhythm.</p>',
          buttons: ['Close']
        });
        await alert.present();
      }

      if (this.focus.includes('extension')) {
        const alert = await this.alertController.create({
          header: 'Definition of Extension',
          mode: 'ios',
          message: '<p>The general carriage should be erect, characterized by an extended bodyline. The angle of the head follows naturally from the line of the back; the arms should be naturally extended with the shoulders down and back.' +
            ' The skater’s hands should follow the line of the movement being executed.' +
            ' The final extended position should be executed in a controlled manner and should achieve the maximum length of all body lines.</p>',
          buttons: ['Close']
        });
        await alert.present();
      }

      if (this.focus.includes('turn execution')) {
        const alert = await this.alertController.create({
          header: 'Definition of Turn Execution',
          mode: 'ios',
          message: '<p>The proper skill and technique of how the turn should be performed.' +
            ' The correct entry and exit edges are to be adequate and maintained throughout the turn for its identification.</p>',
          buttons: ['Close']
        });
        await alert.present();
      }

      if (this.focus.includes('edge quality')) {
        const alert = await this.alertController.create({
          header: 'Definition of Edge Quality',
          mode: 'ios',
          message: '<p>Initiated through proper body alignment over the skating foot, creating a stable arc that travels uninterrupted until a required transition takes place.' +
            ' Depth of edge refers to the acuteness of the arc and is created by the lean of the body and the angle of the blade when it takes the ice.' +
            ' Good edge quality results in a confident, sure and controlled movement.</p>',
          buttons: ['Close']
        });
        await alert.present();
      }

      if (this.focus.includes('continuous flow')) {
        const alert = await this.alertController.create({
          header: 'Definition of Continuous Flow',
          mode: 'ios',
          message: '<p>The skater’s ability to maintain a consistent and undisturbed running edge across the ice.' +
            ' Flow does not necessarily relate to the speed at which the skater is traveling as it is sometimes best recognized as the skater starts to slow.</p>',
          buttons: ['Close']
        });
        await alert.present();
      }

      if (this.focus.includes('posture/carriage')) {
        const alert = await this.alertController.create({
          header: 'Definition of Posture/Carriage',
          mode: 'ios',
          message: '<p>The proper alignment of the hips, back, arms and shoulders, and head over the skate.' +
            ' Unless the move requires a variation, typically, the skater’s back should be straight, with the spine and head perpendicular to the surface of the ice.' +
            ' The arms should be extended out from the shoulders, level and relaxed. The free leg should be in a straight line and slightly turned out from the free hip to the free toe.</p>',
          buttons: ['Close']
        });
        await alert.present();
      }

      if (this.focus.includes('bilateral movement')) {
        const alert = await this.alertController.create({
          header: 'Definition of Bilateral Movement',
          mode: 'ios',
          message: '<p>The ability to execute movements on both sides of the body, clockwise and counterclockwise, forward and backward.</p>',
          buttons: ['Close']
        });
        await alert.present();
      }

      if (this.focus.includes('strength')) {
        const alert = await this.alertController.create({
          header: 'Definition of Strength',
          mode: 'ios',
          message: '<p>The creation and maintenance of balance and flow developed by a continuous rise and fall of the skating knee together with the pressure of the edge of the blade against the ice.' +
            ' The skater should demonstrate the ability to exert equal pressure against the ice with both the right and left foot.' +
            ' End products of strength are' +
            ' (1) good posture;' +
            ' (2) flow across the ice; and' +
            ' (3) consistent pace. </p>',
          buttons: ['Close']
        });
        await alert.present();
      }

    }

    async present2() {
      this.isLoading2 = true;
      return await this.loadingCtrl.create({
        message: '',
      }).then(a => {
        a.present().then(() => {
          if (!this.isLoading2) {
            a.dismiss().then(() => console.log('abort presenting'));
          }
        });
      });
    }
  
    async dismiss2() {
      this.isLoading2 = false;
      return await this.loadingCtrl.dismiss().then(() => console.log('dismissed'));
    }

    goBack(){
      this.location.back();
    }
    }
