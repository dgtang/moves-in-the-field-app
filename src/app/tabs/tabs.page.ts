import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular'
import { AngularFirestore} from '@angular/fire/firestore';
//import { FirebaseAnalytics } from '@ionic-native/firebase-analytics/ngx';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnInit {
  home:boolean = true;
  turn:boolean = true;
  quiz:boolean = true;
  resources:boolean = true;
  store:boolean = true;
  homePage: number
  turnsPage: number
  connectPage: number
  rulebookPage: number
  newPage: number

   //new variables
   newArray = [];
   showBadge
   newUpdateNum
   newNumStored
   arrayLength

  constructor(
    private router: Router,
    public alertController: AlertController,
    public af: AngularFirestore,
    //private firebaseAnalytics: FirebaseAnalytics
  ) { 
    this.home = true;
    this.turn = false;
    this.quiz = false;
    this.resources = false;
    this.store = false;
   
   }

  ngOnInit() {
  }

  goHome(){
    this.home = true;
    this.turn = false;
    this.quiz = false;
    this.resources = false;
    this.store = false;
    this.router.navigate(['tabs/tabs/home']); 
  }

  goQuiz(){
    this.home = false;
    this.turn = false;
    this.quiz = true;
    this.resources = false;
    this.store = false;
  }

  goResources(){
    this.home = false;
    this.turn = false;
    this.quiz = false;
    this.resources = true;
    this.store = false;
  }

  async goTurns(){
    this.home = false;
    this.turn = true;
    this.quiz = false;
    this.resources = false;
    this.store = false;
  }

  async goStore(){
    this.home = false;
    this.turn = false;
    this.quiz = false;
    this.resources = false;
    this.store = true;
    this.showBadge = false
  }

}
