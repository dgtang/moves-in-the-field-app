import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';

import { TabsPageRoutingModule } from './tabs-routing.module';

import { TabsPage } from './tabs.page';

const routes: Routes = [{
    path: '',
    redirectTo: 'tabs/tabs/home',
    pathMatch: 'full'
  },
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'rulebook',
        loadChildren: () => import('../rulebook/rulebook.module').then(m => m.RulebookPageModule)
      },
      {
        path: 'more',
        loadChildren: () => import('../insights/insights.module').then(m => m.InsightsPageModule)
      },
      {
        path: 'turns',
        loadChildren: () => import('../turns/turns.module').then( m => m.TurnsPageModule)
      },
      {
        path: 'home',
        children: [{
            path: '',
            loadChildren: () => import('../home/home.module').then(m => m.HomePageModule)
          },
          {
            path: 'test-details',
            loadChildren: () => import('../test-details/test-details.module').then(m => m.TestDetailsPageModule)
          },
          {
            path: 'pattern-details',
            loadChildren: () => import('../pattern-details/pattern-details.module').then(m => m.PatternDetailsPageModule)
          }
        ]
      },
      {
        path: 'connect',
        loadChildren: () => import('../connect/connect.module').then(m => m.ConnectPageModule)
      },
      {
        path: 'new',
        loadChildren: () => import('../new/new.module').then(m => m.NewPageModule)
      },
      {
        path: '',
        redirectTo: 'tabs/tabs/home',
        pathMatch: 'full'
      }
    ]
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TabsPageRoutingModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TabsPage]
})
export class TabsPageModule {}
