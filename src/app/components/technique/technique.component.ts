import { Component, OnInit, Input } from '@angular/core';
import {ModalController} from '@ionic/angular'

@Component({
  selector: 'app-technique',
  templateUrl: './technique.component.html',
  styleUrls: ['./technique.component.scss'],
})
export class TechniqueComponent implements OnInit {
  @Input("body") body;
  @Input("techniqueDescription") techniqueDescription;
  @Input("tracingVideo") tracingVideo;

  constructor(public modalCtrl: ModalController,) { }

  ngOnInit() {    
    console.log(this.tracingVideo, 'tracingVideo dana');
    
  }

  async goBack() {
    this.modalCtrl.dismiss({
      'dismissed': true
    })
  }

}
