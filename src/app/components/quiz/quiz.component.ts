import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularFirestore} from '@angular/fire/firestore';
import {AlertController, IonContent} from '@ionic/angular'
import { LoadingController, ModalController } from '@ionic/angular';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import * as confetti from 'canvas-confetti';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.scss'],
})
export class QuizComponent implements OnInit {
  @ViewChild(IonContent) content: IonContent;
  @Input("testTitle") testTitle;
  id
  collectionName
  productsPurchased = []
  //question #1
  titlesArray = []
  randomTitlesArray = []

  //question #2
  //1. has bullets still grouped together
  //2. has bullets separated
  //3. has pattern titles and becomes final array 
  //4. randomizes bullets
  //5. spliced array of user answers that you will compareto check array
  bulletsArrayCheck = []
  bulletsArray = []
  randomBulletsArray = []
  numbersTitleArrayBullets = []
  bulletUserAnswerArray = []
  dummyNumbersTitleArrayBullets = []

  //question #3
  //1. has bullets still grouped together
  //2. has bullets separated
  //3. has pattern titles and becomes final array 
  //4. randomizes bullets
  //5. spliced array of user answers that you will compareto check array
  focusArrayCheck = []
  focusArray = []
  randomFocusArray = []
  numbersTitleArrayFocus = []
  focusUserAnswerArray = []
  dummyNumbersTitleArrayFocus = []
  
  QuizPatternContent = [];
  patternNum: number = 1
  goodJobBoolean1: boolean
  tryAgainBoolean1: boolean
  goodJobBoolean2: boolean
  tryAgainBoolean2: boolean
  goodJobBoolean3: boolean
  tryAgainBoolean3: boolean
  //quiz values
  patternName
  patternTwoShuffle: boolean = true
  patternThreeShuffle: boolean = true

  constructor(
    public route: ActivatedRoute,
    public af: AngularFirestore,
    public alertController: AlertController,
    public loadingController: LoadingController,
    public modalController: ModalController,
  ) { 
    this.showLoader().then(()=>{
      //dont think I need commented out stuff below becuase should have happened on instructional-level page, right?
        this.collectionName = 'Patterns_' + this.testTitle.replace(/\s/g, "");
        //this is where specific page content coming from 
        this.af.collection(this.collectionName).ref.orderBy("num")
          .get()
          .then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
              this.QuizPatternContent.push({
                title: doc.data()['title'],
                bullets: doc.data()['bullets'],
                focus: doc.data()['focus'],
                num: doc.data()['num']
              });
            })
          }).then(() => {
  
            for (let index = 0; index < this.QuizPatternContent.length; index++) {
              this.titlesArray.push(this.QuizPatternContent[index].title);
              var numTitle = this.QuizPatternContent[index].num + '. ' + this.QuizPatternContent[index].title
              this.numbersTitleArrayBullets.push(numTitle)
              this.numbersTitleArrayFocus.push(numTitle)
              this.bulletsArrayCheck.push(this.QuizPatternContent[index].bullets)
              this.bulletsArray = this.bulletsArray.concat(this.QuizPatternContent[index].bullets);
              this.focusArrayCheck.push(this.QuizPatternContent[index].focus)
              this.focusArray = this.focusArray.concat(this.QuizPatternContent[index].focus);
            }
  
            //random Title Array
            this.randomTitlesArray = this.randomTitlesArray.concat(this.titlesArray)
            var m = this.randomTitlesArray.length,
              t, i;
            // While there remain elements to shuffle
            while (m) {
              // Pick a remaining element…
              i = Math.floor(Math.random() * m--);
  
              // And swap it with the current element.
              t = this.randomTitlesArray[m];
              this.randomTitlesArray[m] = this.randomTitlesArray[i];
              this.randomTitlesArray[i] = t;
            }
            //bottom
            this.loadingController.dismiss();
          })
      //})
    })
  }

  // Show the loader for infinite time
  async showLoader() {

    const loading = await this.loadingController.create({
      message: '',
   });
   return loading.present();

  }

  drop(event: CdkDragDrop<string[]>) {
    if (this.patternNum == 1) {
      this.goodJobBoolean1 = null
      this.tryAgainBoolean1 = null
    }
    if (this.patternNum == 2) {
      this.goodJobBoolean2 = null
      this.tryAgainBoolean2 = null
    }
    if (this.patternNum == 3) {
      this.goodJobBoolean3 = null
      this.tryAgainBoolean3 = null
    }
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);
    }
  }

    nextBtn() {
      this.content.scrollToTop(500);
      this.patternNum = this.patternNum + 1;
      if (this.patternNum == 2) {
        if(this.patternTwoShuffle == true){
        //random focus Array
        this.randomFocusArray = this.randomFocusArray.concat(this.focusArray)
        var m = this.randomFocusArray.length,
          t, i;
        // While there remain elements to shuffle
        while (m) {
          // Pick a remaining element…
          i = Math.floor(Math.random() * m--);

          // And swap it with the current element.
          t = this.randomFocusArray[m];
          this.randomFocusArray[m] = this.randomFocusArray[i];
          this.randomFocusArray[i] = t;
        }
        //bottom
        this.patternTwoShuffle = false
      }
      }
      if (this.patternNum == 3) {        
        if(this.patternThreeShuffle == true){          
          //random test expectation Array
        this.randomBulletsArray = this.randomBulletsArray.concat(this.bulletsArray)
        var m = this.randomBulletsArray.length,
          t, i;
        // While there remain elements to shuffle
        while (m) {
          // Pick a remaining element…
          i = Math.floor(Math.random() * m--);

          // And swap it with the current element.
          t = this.randomBulletsArray[m];
          this.randomBulletsArray[m] = this.randomBulletsArray[i];
          this.randomBulletsArray[i] = t;
        }
        this.patternThreeShuffle = false
        //bottom
      }
      }
    }

    previousBtn() {
      this.content.scrollToTop(500);
      this.patternNum = this.patternNum - 1;
    }

    startConfetti(){
      var canvas = document.getElementById('my-canvas');

      var myConfetti = confetti.create(canvas, {
        resize: true,
        useWorker: true
      });
  
      myConfetti({
    particleCount: 200,
    spread: 160
  });
    }

    submit1(){
        if (JSON.stringify(this.titlesArray) === JSON.stringify(this.randomTitlesArray)) {
          this.goodJobBoolean1 = true
          this.tryAgainBoolean1 = false
          this.startConfetti()
        } else {
          this.goodJobBoolean1 = false
          this.tryAgainBoolean1 = true
        }
    }

    submit2() {
      this.focusUserAnswerArray = []
      this.dummyNumbersTitleArrayFocus = []
      // var result=[];
      this.focusUserAnswerArray = this.focusUserAnswerArray.concat(this.numbersTitleArrayFocus);
      for (var i = 0; i < this.focusUserAnswerArray.length; i++) {
        if (this.focusUserAnswerArray[i].charAt(1) == '.') {
          this.focusUserAnswerArray.splice(i, 1)
        }
      }

      for (let index = 0; index < this.focusArrayCheck.length; index++) {
        this.dummyNumbersTitleArrayFocus.push(this.focusUserAnswerArray.splice(0, this.focusArrayCheck[index].length));
      }

      for (let index = 0; index < this.dummyNumbersTitleArrayFocus.length; index++) {

        let res1 = this.dummyNumbersTitleArrayFocus[index].filter(item => !this.focusArrayCheck[index].includes(item));
        if ((res1.length != 0)) {
          this.goodJobBoolean2 = false
          this.tryAgainBoolean2 = true
        }
        if (this.dummyNumbersTitleArrayFocus.length - 1 === index) {
          if (this.goodJobBoolean2 != false) {
            this.goodJobBoolean2 = true
            this.tryAgainBoolean2 = false
            this.startConfetti()
          }
        }
      }
    }

    submit3() {
      this.bulletUserAnswerArray = []
      this.dummyNumbersTitleArrayBullets = []
      this.bulletUserAnswerArray = this.bulletUserAnswerArray.concat(this.numbersTitleArrayBullets);
      //console.log(this.bulletUserAnswerArray, 'this.bulletUserAnswerArray');
      
      for (var i = 0; i < this.bulletUserAnswerArray.length; i++) {
        if (this.bulletUserAnswerArray[i].charAt(1) == '.') {
          this.bulletUserAnswerArray.splice(i, 1)
          //console.log(this.bulletUserAnswerArray, 'this.bulletUserAnswerArray splice');
        }
      }

      for (let index = 0; index < this.bulletsArrayCheck.length; index++) {
        this.dummyNumbersTitleArrayBullets.push(this.bulletUserAnswerArray.splice(0, this.bulletsArrayCheck[index].length));
        //console.log(this.dummyNumbersTitleArrayBullets, 'this.dummyNumbersTitleArrayBullets');
      }
      for (let index = 0; index < this.dummyNumbersTitleArrayBullets.length; index++) {
        let res1 = this.dummyNumbersTitleArrayBullets[index].filter(item => !this.bulletsArrayCheck[index].includes(item));
        //console.log(res1, 'res1');
        if ((res1.length != 0)) {
          //console.log('res 1 does not equal 0');
          
          this.goodJobBoolean3 = false
          this.tryAgainBoolean3 = true
        }
        if (this.dummyNumbersTitleArrayBullets.length - 1 === index) {
          if (this.goodJobBoolean3 != false) {
            this.goodJobBoolean3 = true
            this.tryAgainBoolean3 = false
            this.startConfetti()
          }
        }
      }
    }

    ngOnInit() {
    }

    async exitQuiz() {
      const alert = await this.alertController.create({
        header: 'Are you sure?',
        mode: 'ios',
        message: '<p class="alertText">No answers will be saved once you exit this quiz.</p>',
        buttons: [{
            text: 'Cancel'
          },
          {
            text: 'Exit',
            handler: () => {
              this.modalController.dismiss({
                'dismissed': true
              })
            }
          }
        ]
      });
      await alert.present();
    }

    }
