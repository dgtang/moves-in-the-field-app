import { Component, OnInit, Input,ChangeDetectorRef } from '@angular/core';
import { AlertController, ModalController, LoadingController, Platform} from '@ionic/angular';
import { InAppPurchase2, IAPProduct } from '@ionic-native/in-app-purchase-2/ngx';
import { QuizComponent } from '../quiz/quiz.component';
import { ExercisesComponent } from '../exercises/exercises.component';
import { InstructionComponent } from '../instruction/instruction.component';
import { ErrorsComponent } from '../errors/errors.component';
import { TurnSheetComponent } from '../turn-sheet/turn-sheet.component';
import { ReadyComponent } from '../ready/ready.component';
import { IntroduceComponent } from '../introduce/introduce.component';
import { PlacementComponent } from '../placement/placement.component';
import { SafeResourceUrl } from '@angular/platform-browser';
import { AngularFirestore} from '@angular/fire/firestore';

@Component({
  selector: 'app-unlock',
  templateUrl: './unlock.component.html',
  styleUrls: ['./unlock.component.scss'],
})
export class UnlockComponent implements OnInit {
  @Input("unlockBullets") unlockBullets; 
  @Input("unlockPreview") unlockPreview;
  @Input("productDisplayName") productDisplayName; 
  @Input("OFFICIAL_PROD_SELECTED") OFFICIAL_PROD_SELECTED; 
  @Input("productType") productType; 
  @Input("collectionName") collectionName; 
  @Input("price") price; 
  @Input("id") id;
  @Input("turnName") turnName;
  @Input("testName") testName;
  @Input("allLevelsPrice") allLevelsPrice;
  productsPurchased = []
  componentName;
  unlockPreviewURL: SafeResourceUrl
  saveDollarAmount
  regularValue


  constructor(
    public alertController: AlertController,
    private store: InAppPurchase2,
    public modalCtrl: ModalController,
    public loadingController: LoadingController,
    private plt: Platform,
    public af: AngularFirestore,
    private ref: ChangeDetectorRef
  ) { 
    console.log = function(){};
  }

  ngOnInit() {
    this.af.collection("Links").doc('save').ref.get().then((doc) =>{
      console.log(doc.data()['save'], 'doc.data()[s');
      this.saveDollarAmount = doc.data()['save'];
      this.regularValue = doc.data()['regularValue'];
    });
  }

  async restoreAlert(value: any) {
    const alert = await this.alertController.create({
      header: 'Already purchased?',
      mode: 'ios',
      message: '<p class="alertText">If you already purchased this item, but it is not available to you, click restore to restore it to your device.</p>',
      buttons: [{
          text: 'Cancel'
        },
        {
          text: 'Restore',
          handler: () => {
            this.restore(value)
          }
        }
      ]
    });
    await alert.present();
  }

  async promoAlert(value: any){
    if (this.plt.is('ios')) {
    const alert = await this.alertController.create({
      header: 'Have a promo code?',
      mode: 'ios',
      message: 
      '<p>1. Open App Store app.</p>'+
      '<p>2. Click top right Circle "account" Icon.</p>'+
      '<p>3. Click "Redeem Gift Card or Code".</p>'+
      '<p>4. Click "Enter Code Manually" and type code.</p>'+
      '<p>5. Come back to MITF app. Click "Activate Code" below to get access to the promo code content.</p>',
      // '<p class="alertText">Open your App Store app. Click on the top right Circle "account" Icon. Click "Redeem Gift Card or Code". Click "Enter Code Manually" and type code. Come back to MITF app. Click "Activate Code" below to get access to the promo code content.</p>',
      buttons: [{
          text: 'Cancel'
        },
        {
          text: 'Activate Code',
          handler: () => {
            this.restore(value)
          }
        }
      ]
    });
    await alert.present();
  }
  if (this.plt.is('android')) {
    const alert = await this.alertController.create({
      header: 'Have a promo code?',
      mode: 'ios',
      message: 
      '<p>1. Open Google Play app.</p>'+
      '<p>2. Click top left menu icon.</p>'+
      '<p>3. Click "Redeem" and type code.</p>'+
      '<p>4. Come back to MITF app. Click "Activate Code" below to get access to the promo code content.</p>',
      // '<p class="alertText">Open your App Store app. Click on the top right Circle "account" Icon. Click "Redeem Gift Card or Code". Click "Enter Code Manually" and type code. Come back to MITF app. Click "Activate Code" below to get access to the promo code content.</p>',
      buttons: [{
          text: 'Cancel'
        },
        {
          text: 'Activate Code',
          handler: () => {
            this.restore(value)
          }
        }
      ]
    });
    await alert.present();
  }
  }

  buyItem(value: any) {
   // alert('in buy item*')
    console.log(this.OFFICIAL_PROD_SELECTED, 'this.OFFICIAL_PROD_SELECTED dana in buy tiem');
      this.modalCtrl.dismiss({
        'dismissed': true
      });
        console.log('dana #1 unlock page');
        if(value == "allLevels"){
          //alert('in all levels**')
          this.OFFICIAL_PROD_SELECTED = 'com.movesofficial.alllevels'
          console.log(this.OFFICIAL_PROD_SELECTED, 'this.OFFICIAL_PROD_SELECTED dana in buy all levels');
          //alert( this.OFFICIAL_PROD_SELECTED)//*** */
          console.log('dana in all levels if statement on unlock page #23');
          this.store.order(this.OFFICIAL_PROD_SELECTED).then((p) => {
            this.store.when(this.OFFICIAL_PROD_SELECTED)
            .approved((p: IAPProduct) => {
              //alert('item approved...not getting here')
              console.log('dana item purchased approved #3');
              this.storeItem()
              this.ref.detectChanges();
      
              return p.verify();
            })
            .verified((p: IAPProduct) => p.finish());
          })
        }
        if(value == "individualPurchase"){
          //alert('in individual purchase**')
          console.log('dana in individualPurchase if statement on unlock page #24');
          this.store.order(this.OFFICIAL_PROD_SELECTED).then((p) => {
            this.store.when(this.OFFICIAL_PROD_SELECTED)
            .approved((p: IAPProduct) => {
              //alert('item approved...inindividual purchase')
              console.log('dana item purchased approved #1');
              this.storeItem()
              this.ref.detectChanges();
              return p.verify();
            })
            .verified((p: IAPProduct) => p.finish());
          })
        }
  }

  storeItem(){
    //alert('in store item function')
    console.log(this.OFFICIAL_PROD_SELECTED, 'this.OFFICIAL_PROD_SELECTED dana in store tiem');
    console.log('dana in store products --loop 2');
      this.productsPurchased = JSON.parse(localStorage.getItem('productsPurchased'))
      console.log(this.productsPurchased, 'dana products purchased parsed');
        if ((this.productsPurchased != undefined) || (this.productsPurchased != null)) {  
          //if user has purchased an item and it has not been stored yet 
          if ((this.productsPurchased.indexOf(this.OFFICIAL_PROD_SELECTED) == -1)) {
            console.log('dana item has not yet been stored and is about to be stored');
            console.log('dana not undefined --loop 3');
            this.productsPurchased.push(this.OFFICIAL_PROD_SELECTED);
            localStorage.setItem('productsPurchased', JSON.stringify(this.productsPurchased));
            this.goToPage()
          } 
        }
        if ((this.productsPurchased == undefined) || (this.productsPurchased == null)) {  
          console.log('dana in undefined');
          this.productsPurchased = []
          this.productsPurchased.push(this.OFFICIAL_PROD_SELECTED);
          localStorage.setItem('productsPurchased', JSON.stringify(this.productsPurchased));
          this.goToPage()
        }
  }

  goToPage(){
    console.log('dana in open whatever clicked --loop 1');
    if(this.productType == 'quiz'){
      this.openQuiz()
    }
    if(this.productType == 'checklist'){
      this.openChecklist()
    }
    if(this.productType == 'exercise'){
      this.componentName = ExercisesComponent
      this.openInstructional()
    }
    if(this.productType == 'error'){
      this.componentName = ErrorsComponent
      this.openInstructional()
    }
    if(this.productType == 'detail'){
      this.componentName = InstructionComponent
      this.openInstructional()
    }
    if(this.productType == 'introduce'){
      this.componentName = IntroduceComponent
      this.openInstructional()
    }
    if(this.productType == 'placement'){
      this.componentName = PlacementComponent
      this.openInstructional()
    }
    if(this.productType == 'ready'){
      this.componentName = ReadyComponent
      this.openInstructional()
    }
  }


  async openQuiz(){
    const modal = await this.modalCtrl.create({
      component: QuizComponent,
      cssClass: "modal-fullscreen",
      componentProps: {
        testTitle: this.productDisplayName.replace(' Quiz','')
      }
    });
    return await modal.present();
  }

  async openChecklist(){
    const modal = await this.modalCtrl.create({
      component: TurnSheetComponent,
      cssClass: "modal-fullscreen",
      componentProps: {
        id: this.id,
        turnName: this.turnName
      }
    });
    return await modal.present();
  }

  async openInstructional(){
    const modal = await this.modalCtrl.create({
      component: this.componentName,
      cssClass: "modal-fullscreen",
      componentProps: {
        id: this.id,
        collectionName: this.collectionName,
        pullFromDatabase: 'yes' 
      }
    });
    return await modal.present();
  }

  //this is basically for user with promo code...the code on the other pages makes the restore button basically useless
  restore(value) {
    console.log(value, 'dana restore type value');
    console.log(this.OFFICIAL_PROD_SELECTED, 'this.OFFICIAL_PROD_SELECTED dana in restore tiem');
    this.modalCtrl.dismiss({
      'dismissed': true
    });
    console.log('dana in restore function');
    if(value == 'allLevels'){
      this.OFFICIAL_PROD_SELECTED = 'com.movesofficial.alllevels'
      console.log(this.OFFICIAL_PROD_SELECTED, 'this.OFFICIAL_PROD_SELECTED dana in buy all levels');
      this.store.when('com.movesofficial.alllevels')
      .approved((p: IAPProduct) => {
        // Handle the product deliverable
        console.log(JSON.stringify(p), 'dana ??');
        this.ref.detectChanges();
        return p.verify();
      })
      .verified((p: IAPProduct) => p.finish());

      this.store.when('com.movesofficial.alllevels').owned((p: IAPProduct) => {
        console.log(JSON.stringify(p), 'dana Im not sure what is going on');//this worked!!!!
        if (p.owned){
          //then show them product and store it to local storage
          this.storeItem()
        }
        else{
         alert('No previous purchase or promo code found')
        }
      });
      this.store.refresh();
    }

    if(value == 'individualPurchase'){
      this.store.when(this.OFFICIAL_PROD_SELECTED)
      .approved((p: IAPProduct) => {
        // Handle the product deliverable
        console.log(JSON.stringify(p), 'dana individual purchase??');
        this.ref.detectChanges();
        return p.verify();
      })
      .verified((p: IAPProduct) => p.finish())

      this.store.when(this.OFFICIAL_PROD_SELECTED).owned((p: IAPProduct) => {
        console.log(JSON.stringify(p), 'dana Im not sure what is going on 2');//this worked!!!!
        if (p.owned){
       //then show them product and store it to local storage
       this.storeItem()
      }
      else{
        alert('No previous purchase or promo code found')
      }
      });
      this.store.refresh();
    }  
  }

 cancel() {
  this.modalCtrl.dismiss({
    'dismissed': true
  });
}



}
