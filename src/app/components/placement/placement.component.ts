import { Component, OnInit, Input } from '@angular/core';
import { AngularFirestore} from '@angular/fire/firestore';
import { ModalController, LoadingController, AlertController } from '@ionic/angular';
import { PlacementDefinitionsComponent } from '../placement-definitions/placement-definitions.component';

@Component({
  selector: 'app-placement',
  templateUrl: './placement.component.html',
  styleUrls: ['./placement.component.scss'],
})
export class PlacementComponent implements OnInit {
  @Input("collectionName") collectionName;
  @Input("id") id;
  patternAxis
  numLobes
  moreInfoVideo
  moreInfo
  numLobesNumber

  constructor(
    public af: AngularFirestore,
    public modalController: ModalController,
    public alertController: AlertController,
    public loadingCtrl: LoadingController,
  ) { }

  ngOnInit() {
    this.showLoader().then(() => {
    this.af.collection(this.collectionName).doc(this.id).ref.get().then((doc) => {
      this.patternAxis = doc.data()['patternAxis']
      this.numLobes = doc.data()['numLobes']
      this.moreInfo = doc.data()['moreInfo']
      this.moreInfoVideo = doc.data()['moreInfoVideo']
      this.numLobesNumber = doc.data()['numLobesNumber']
      this.loadingCtrl.dismiss();
    })
  })
  }

  async showLoader() {
    const loading = await this.loadingCtrl.create({
      message: '',
    });
    return loading.present();

  }

  async goBack() {
    this.modalController.dismiss({
      'dismissed': true
    })
  }

  async openModal(){
    const modal = await this.modalController.create({
      component: PlacementDefinitionsComponent,
      cssClass: "modal-fullscreen",
    });
    return await modal.present();
  }

}
