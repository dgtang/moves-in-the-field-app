import { Component, OnInit, Input } from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import { ActivatedRoute } from '@angular/router';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-resource',
  templateUrl: './resource.component.html',
  styleUrls: ['./resource.component.scss'],
})
export class ResourceComponent implements OnInit {
  @Input("item") item;
  testFormsURL;
  checkListURL;
  judgePointsURL
  rulebookURL
  rulebookPages = [];
  
  constructor(
    public af: AngularFirestore,
    public route: ActivatedRoute,
    public modalController: ModalController,
  ) { }

  ngOnInit() {
    if(this.item == 'formResource'){
      this.af.collection("Links").doc('JudgeForms').ref.get().then((doc) =>{
        this.testFormsURL = doc.data()['link'];
      });
    }
    if(this.item == 'testResource'){
      this.af.collection("Links").doc('SkaterChecklist').ref.get().then((doc) =>{
        this.checkListURL = doc.data()['link'];
      });
    }
    if(this.item == 'handbookResource'){
      this.af.collection("Links").doc('judgePoints').ref.get().then((doc) =>{
        this.judgePointsURL = doc.data()['link'];
      });
    } 
  }

  async goBack() {
    this.modalController.dismiss({
      'dismissed': true
    })
  }

}
