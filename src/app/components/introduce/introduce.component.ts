import { Component, OnInit, Input } from '@angular/core';
import { AngularFirestore} from '@angular/fire/firestore';
import { ModalController, LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-introduce',
  templateUrl: './introduce.component.html',
  styleUrls: ['./introduce.component.scss'],
})
export class IntroduceComponent implements OnInit {
  @Input("collectionName") collectionName;
  @Input("id") id;
  introductionArray

  constructor(
    public af: AngularFirestore,
    public modalController: ModalController,
    public loadingCtrl: LoadingController,
  ) { }

  ngOnInit() {
    this.showLoader().then(() => {
    this.af.collection(this.collectionName).doc(this.id).ref.get().then((doc) => {
      this.introductionArray = doc.data()['introduction'].split('.')
      this.loadingCtrl.dismiss();
    })
  })
  }

  async showLoader() {
    const loading = await this.loadingCtrl.create({
      message: '',
    });
    return loading.present();

  }

  async goBack() {
    this.modalController.dismiss({
      'dismissed': true
    })
  }

}
