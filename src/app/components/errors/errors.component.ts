import { Component, OnInit, Input } from '@angular/core';
import { AngularFirestore} from '@angular/fire/firestore';
import {AlertController, LoadingController} from '@ionic/angular'
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-errors',
  templateUrl: './errors.component.html',
  styleUrls: ['./errors.component.scss'],
})
export class ErrorsComponent implements OnInit {
  @Input("collectionName") collectionName;
  @Input("id") id;
  errorsArray = []
  correctionsArray = []

  constructor(
    public af: AngularFirestore,
    public alertController: AlertController,
    public modalController: ModalController,
    public loadingCtrl: LoadingController,
    ) { 
  }

  ngOnInit() {
    this.showLoader().then(() => {
      this.af.collection(this.collectionName).doc(this.id).ref.get().then((doc) => {
        this.errorsArray.push({
          corrections: doc.data()['corrections'],
          errors: doc.data()['errors'],
        });
      })
      .then(() => {        
        this.loadingCtrl.dismiss();
      })
    })
  }

  async showLoader() {

    const loading = await this.loadingCtrl.create({
      message: '',
    });
    return loading.present();

  }

  async goBack() {
    this.modalController.dismiss({
      'dismissed': true
    })
  }

}
