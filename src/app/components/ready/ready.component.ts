import { Component, OnInit } from '@angular/core';
import { AngularFirestore} from '@angular/fire/firestore';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-ready',
  templateUrl: './ready.component.html',
  styleUrls: ['./ready.component.scss'],
})
export class ReadyComponent implements OnInit {

  constructor(
    public af: AngularFirestore,
    public modalController: ModalController,
  ) { }

  ngOnInit() {
  }

  async goBack() {
    this.modalController.dismiss({
      'dismissed': true
    })
  }

}
