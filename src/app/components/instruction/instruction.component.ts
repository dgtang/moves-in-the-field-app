import { Component, OnInit, Input } from '@angular/core';
import { AngularFirestore} from '@angular/fire/firestore';
import { ModalController, LoadingController } from '@ionic/angular';
import { AuthService } from '../../services/auth.service';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-instruction',
  templateUrl: './instruction.component.html',
  styleUrls: ['./instruction.component.scss'],
})
export class InstructionComponent implements OnInit {
  @Input("collectionName") collectionName;
  @Input("id") id;
  techniqueTitles = []
  techniques = []
  slowVideos = []
  watchFors = []
  blades = []
  variations = []
  moreTechniqueInfos = []
  techniqueTitle
  technique
  slowVideo: SafeResourceUrl;
  watchFor = []
  blade = []
  variation = []
  moreTechniqueInfo = []
  arrayIndex:number = 0;
  numParts = []
  chosen = "0";
  showLongBladeSection

  
  constructor(
    public af: AngularFirestore,
    public modalController: ModalController,
    public loadingCtrl: LoadingController,
    private authService: AuthService,
    private domSanitizer: DomSanitizer
  ) { 
    this.authService.changeStuffShown$.subscribe((passingArray) => {
      console.log('in constructor');
      console.log(passingArray[0], 'this.passingArray instruction page');
      this.technique = passingArray[0].technique
      this.techniqueTitle = passingArray[0].techniqueTitle
      this.slowVideo = passingArray[0].slowVideo
      this.watchFor = passingArray[0].watchFor.split('.')
      this.blade = passingArray[0].blade.split('.')
      if(passingArray[0].variation != undefined){
        this.variation = passingArray[0].variation.split('.')
      }
      if(passingArray[0].moreTechniqueInfo != undefined){
        this.moreTechniqueInfo = passingArray[0].moreTechniqueInfo.split('.')
      }
    })
  }

  ngOnInit() {
    //if this doesnt work move it to the constructor
    this.showLoader().then(() => {
      this.af.collection(this.collectionName).doc(this.id).ref.get().then((doc) => {
      //plural
      this.techniques = doc.data()['technique']
      this.techniqueTitles = doc.data()['techniqueTitles']
      this.slowVideos = doc.data()['slowVideos'] 
      this.watchFors = doc.data()['watchFor']
      this.blades = doc.data()['blade']
      if(doc.data()['variations'] != undefined){
        this.variations = doc.data()['variations']
      }
      if(doc.data()['moreTechniqueInfo'] != undefined){
      this.moreTechniqueInfos = doc.data()['moreTechniqueInfo']
      }
      
      //singular
      this.technique = doc.data()['technique'][0]
      this.techniqueTitle = doc.data()['techniqueTitles'][0]
      this.slowVideo = this.domSanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/'+ doc.data()['slowVideos'][0] +'?rel=0&loop=1&modestbranding=1&showinfo=0&autoplay=0')
      this.watchFor = doc.data()['watchFor'][0].split('.')
      this.blade = doc.data()['blade'][0].split('.')
      if(doc.data()['variations'] != undefined){
        this.variation = doc.data()['variations'][0].split('.')
      }
      if(doc.data()['moreTechniqueInfo'] != undefined){
      this.moreTechniqueInfo = doc.data()['moreTechniqueInfo'][0].split('.')
      }
      this.loadingCtrl.dismiss();
    })
    .then(()=>{
      console.log(this.technique, this.techniqueTitle, this.slowVideo, this.watchFor, this.blade, this.variation, this.moreTechniqueInfo, this.variation[this.arrayIndex], this.moreTechniqueInfo[this.arrayIndex], 'on instruction page');
      
      for (let index = 0; index < this.techniqueTitles.length; index++) {
        this.numParts.push(JSON.stringify(index))  
      }
    })
  })
  }

  segmentChanged(ev: any) {
    console.log(ev.detail.value, 'ev');
    this.arrayIndex = ev.detail.value
    console.log(this.arrayIndex, 'this.arrayIndex');
    this.authService.changeStuffShown(
      this.techniques, 
      this.techniqueTitles, 
      this.slowVideos,
      this.watchFors,
      this.blades,
      this.variations,
      this.moreTechniqueInfos,
      this.arrayIndex
      );
  }

  async showLoader() {
    const loading = await this.loadingCtrl.create({
      message: '',
    });
    return loading.present();
  }

  async goBack() {
    this.modalController.dismiss({
      'dismissed': true
    })
  }

  
}
