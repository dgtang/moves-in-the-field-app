import { Component, OnInit, Input } from '@angular/core';
import { AngularFirestore} from '@angular/fire/firestore';
import {AlertController, LoadingController} from '@ionic/angular'
import { ModalController } from '@ionic/angular';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { NgIf } from '@angular/common';

@Component({
  selector: 'app-exercises',
  templateUrl: './exercises.component.html',
  styleUrls: ['./exercises.component.scss'],
})
export class ExercisesComponent implements OnInit {
  @Input("collectionName") collectionName;
  @Input("id") id;
  @Input("bullets") bullets;
  @Input("pullFromDatabase") pullFromDatabase;
  exerciseArray = []
  exerciseVideoArray = []
  exerciseVideoArrayHolder = []
  exerciseSplitArray = []
  exerciseVideosSplitArray = []

  constructor(
    public af: AngularFirestore,
    public alertController: AlertController,
    public modalController: ModalController,
    public loadingCtrl: LoadingController,
    private domSanitizer: DomSanitizer
  ) {     
  }

  ngOnInit() {
      this.showLoader().then(() => {
        this.af.collection(this.collectionName).doc(this.id).ref.get().then((doc) => {
          this.exerciseArray = doc.data()['exercises']
          this.exerciseVideoArrayHolder = doc.data()['exerciseVideos']
          if(this.pullFromDatabase == 'yes'){
            this.bullets = doc.data()['bullets']
          }
        }).then(() => {
          for (let index = 0; index < this.exerciseVideoArrayHolder.length; index++) {
            if((this.exerciseVideoArrayHolder[index] == 'null') || (this.exerciseVideoArrayHolder[index] == 'N/A')){
              this.exerciseVideoArray.push(this.exerciseVideoArrayHolder[index])
           }
            else{
              this.exerciseVideoArray.push(this.domSanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/'+ this.exerciseVideoArrayHolder[index] +'?rel=0&loop=1&modestbranding=1&showinfo=0&autoplay=0'))
            }
          }
          console.log(this.exerciseVideoArray, 'this.exerciseVideoArray');
          
        })
        .then(() => {
          var perChunk = 2

          //exercise text split 
          this.exerciseSplitArray = this.exerciseArray.reduce((resultArray, item, index) => { 
            const chunkIndex = Math.floor(index/perChunk)
        
            if(!resultArray[chunkIndex]) {
              resultArray[chunkIndex] = [] // start a new chunk
            }
          
            resultArray[chunkIndex].push(item)
          
            return resultArray
          }, [])

          //exercise text split 
          this.exerciseVideosSplitArray = this.exerciseVideoArray.reduce((resultArray, item, index) => { 
            const chunkIndex = Math.floor(index/perChunk)
        
            if(!resultArray[chunkIndex]) {
              resultArray[chunkIndex] = [] // start a new chunk
            }
          
            resultArray[chunkIndex].push(item)
          
            return resultArray
          }, [])
          
          this.loadingCtrl.dismiss();
          console.log(this.exerciseSplitArray, 'this.exerciseSplitArray');
          console.log(this.exerciseVideosSplitArray[0][0], 'this.exerciseVideosSplitArray');
          
         })
      })
  }

  async showLoader() {
    const loading = await this.loadingCtrl.create({
      message: '',
    });
    return loading.present();

  }

  async goBack() {
    this.modalController.dismiss({
      'dismissed': true
    })
  }

}
