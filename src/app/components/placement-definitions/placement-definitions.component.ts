import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-placement-definitions',
  templateUrl: './placement-definitions.component.html',
  styleUrls: ['./placement-definitions.component.scss'],
})
export class PlacementDefinitionsComponent implements OnInit {
  continuousVideo: SafeResourceUrl;
  longVideo: SafeResourceUrl;
  shortVideo: SafeResourceUrl;
  diagonalVideo: SafeResourceUrl;
  transverseVideo: SafeResourceUrl;
  

  
  constructor( 
    public modalController: ModalController,
    private domSanitizer: DomSanitizer
    ) {  
      this.continuousVideo = this.domSanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/mnis2Ii8RxM?rel=0&loop=1&modestbranding=1&showinfo=0&autoplay=0')
      this.longVideo = this.domSanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/BisTKNGxLyo?rel=0&loop=1&modestbranding=1&showinfo=0&autoplay=0')
      this.shortVideo = this.domSanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/HcTM6x5bypY?rel=0&loop=1&modestbranding=1&showinfo=0&autoplay=0')
      this.diagonalVideo = this.domSanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/s0HeFXcnSNs?rel=0&loop=1&modestbranding=1&showinfo=0&autoplay=0')
      this.transverseVideo = this.domSanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/gSWccVMDg88?rel=0&loop=1&modestbranding=1&showinfo=0&autoplay=0')
     }

  ngOnInit() {
   
  }

  async goBack() {
    this.modalController.dismiss({
      'dismissed': true
    })
  }


}
