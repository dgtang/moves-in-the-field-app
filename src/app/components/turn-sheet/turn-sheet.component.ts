import { Component, OnInit, Input } from '@angular/core';
import {ModalController, LoadingController, AlertController, Platform} from '@ionic/angular'
import { AngularFirestore} from '@angular/fire/firestore';
import { TechniqueComponent } from '../technique/technique.component';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-turn-sheet',
  templateUrl: './turn-sheet.component.html',
  styleUrls: ['./turn-sheet.component.scss'],
})
export class TurnSheetComponent implements OnInit {
  @Input("id") id;
  @Input("turnName") turnName;
  levelArray = []
  closedLevelArray = []
  //videos
  video1: SafeResourceUrl;
  video2: SafeResourceUrl;
  video1Closed: SafeResourceUrl;
  video2Closed: SafeResourceUrl; 
  videoTracing: SafeResourceUrl; 
  videoTracingClosed: SafeResourceUrl;
  varCapVideo: SafeResourceUrl;
  varCircleTheoryVideo: SafeResourceUrl;
  //videos above
  turnTitle1
  turnTitle2
  turnDefinition
  closedTurnDefinition
  openTurnDefinition
  turnType
  tracingNote
  file
  note = []
  techniqueDescription
  body
  techniqueArray = []
  closedTechniqueArray = []
  turnTitle1Select:boolean = true;
  turnTitle2Select:boolean = true;
  //turn definition
  feet
  edge
  lobe
  turnDirection
  turnPoint
  turnSize
  segmentModel = "closed";
  tips = []
  turnL
  turnR
  turnTracing
  isIpad = false

  constructor(
    public modalCtrl: ModalController,
    public af: AngularFirestore,
    public loadingCtrl: LoadingController,
    public alertController: AlertController,
    private domSanitizer: DomSanitizer,
    private plt: Platform,
  ) { 
    if((this.plt.is('ios')) && (this.plt.is('ipad'))){
      this.isIpad = true
    }

    this.turnTitle1Select = true;
    this.turnTitle2Select = false;

  }

  segmentChanged(ev: any) {
    if(ev.detail.value == "open"){
      this.turnTitle1Select = true;
      this.turnTitle2Select = false;
    }
    else{
      this.turnTitle1Select = false;
      this.turnTitle2Select = true;
    }
  }

  ngOnInit() {
    this.showLoader().then(() => {
    this.af.collection("Turns").doc(this.id).ref.get().then((doc) =>{
      this.video1 = this.domSanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/'+ doc.data()['videosYoutube'][1] +'?rel=0&loop=1&modestbranding=1&showinfo=0&autoplay=0')
      this.video2 = this.domSanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/'+ doc.data()['videosYoutube'][2] +'?rel=0&loop=1&modestbranding=1&showinfo=0&autoplay=0')
      this.videoTracing = this.domSanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/'+ doc.data()['videosYoutube'][0] +'?rel=0&loop=1&modestbranding=1&showinfo=0&autoplay=0')
      if(doc.data()['videos2Youtube'] != null){
      this.video1Closed = this.domSanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/'+ doc.data()['videos2Youtube'][1] +'?rel=0&loop=1&modestbranding=1&showinfo=0&autoplay=0')
      this.video2Closed = this.domSanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/'+ doc.data()['videos2Youtube'][2] +'?rel=0&loop=1&modestbranding=1&showinfo=0&autoplay=0')
      this.videoTracingClosed = this.domSanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/'+ doc.data()['videos2Youtube'][0] +'?rel=0&loop=1&modestbranding=1&showinfo=0&autoplay=0')
      }
      if(this.turnName.includes('Loop')){
      this.varCapVideo = this.domSanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/'+ doc.data()['varCapVideo'] +'?rel=0&loop=1&modestbranding=1&showinfo=0&autoplay=0')
      this.varCircleTheoryVideo = this.domSanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/'+ doc.data()['varCircleTheoryVideo'] +'?rel=0&loop=1&modestbranding=1&showinfo=0&autoplay=0')
      }
      this.levelArray = doc.data()['levels'];
      this.turnTitle1 = doc.data()['turnTitle1'];
      this.turnTitle2 = doc.data()['turnTitle2'];
      this.note = doc.data()['note'];
      this.techniqueArray = doc.data()['techniqueArray'];
      this.closedTechniqueArray = doc.data()['closedTechniqueArray'];
      this.tracingNote = doc.data()['tracingNote'];
      this.closedLevelArray = doc.data()['closedLevelArray'];
      this.tips = doc.data()['tips'];
    }).then(() => {        
      this.loadingCtrl.dismiss();
    })
  })

    if (this.turnName.includes('Mohawk')) {
      if(this.turnName.includes('Outside')){
        this.edge = 'outside to outside'
      }
      else{
        this.edge = 'inside to inside'
      }
      this.turnType = 'Mohawk'
      this.feet = 'perform on one foot to the other'
      this.lobe = 'entry and exit edge are continuous and of equal depth on same lobe'
      this.turnDirection = 'turn same direction as your entry edge (if entry edge is clockwise, turn clockwise)'
      this.turnPoint = ''
      this.turnSize = ''
      this.openTurnDefinition = 'The heel of the free foot is placed on the ice at the inner side of the skating foot, the angle between the two feet being optional. Following the weight transfer, the immediate position of the new free foot is behind the heel of the new skating foot.'
}

    if (this.turnName.includes('3-turn')) {
      if(this.turnName.includes('Outside')){
        this.edge = 'outside to inside'
      }
      else{
        this.edge = 'inside to outside'
      }
      this.turnType = '3-turn'
      this.feet = 'perform on 1 foot'
      this.lobe = 'entry and exit edge on same lobe'
      this.turnDirection = 'turn same direction as your entry edge (if entry edge is clockwise, turn clockwise)'
      this.turnPoint = 'cusp of turn points into lobe'
      this.turnSize = ''
    }

    if (this.turnName.includes('Choctaw')) {
      if(this.turnName.includes('Outside')){
        this.edge = 'outside to inside'
      }
      else{
        this.edge = 'inside to outside'
      }
      this.turnType = 'Choctaw'
      this.feet = 'perform on one foot to the other'
      this.lobe = 'entry and exit edge are of equal depth on different lobes'
      this.turnDirection = 'entry edge is opposite of exit edge (if entry edge is clockwise, exit edge is counterclockwise)'
      this.turnPoint = ''
      this.turnSize = ''
      this.closedTurnDefinition = 'The instep of the free foot is brought to the heel of the skating foot until the free foot is placed on the ice behind the heel of the skating foot. Following the weight transfer the immediate position of the new free foot is in front of the new skating foot.'
      this.openTurnDefinition = 'The free foot is placed on the ice on the inner side of the skating foot. Following the weight transfer the immediate position of the new free foot is behind the heel of the new skating foot.'
}

    if (this.turnName.includes('Bracket')) {
      if(this.turnName.includes('Outside')){
        this.edge = 'outside to inside'
      }
      else{
        this.edge = 'inside to outside'
      }
      this.turnType = 'Bracket'
      this.feet = 'perform on 1 foot'
      this.lobe = 'entry and exit edge on same lobe'
      this.turnDirection = 'turn opposite direction of your entry edge (if entry edge is clockwise, turn counterclockwise)'
      this.turnPoint = 'cusp of turn points outside of lobe'
      this.turnSize = ''
    }

    if (this.turnName.includes('Counter')) {
      if(this.turnName.includes('Outside')){
        this.edge = 'outside to outside'
      }
      else{
        this.edge = 'inside to inside'
      }
      this.turnType = 'Counter'
      this.feet = 'perform on 1 foot'
      this.lobe = 'entry and exit edge on two different lobes'
      this.turnDirection = 'turn opposite direction of your entry edge (if entry edge is clockwise, turn counterclockwise)'
      this.turnPoint = 'cusp of turn points outwards from first lobe'
      this.turnSize = ''
    }

    if (this.turnName.includes('Loop')) {
      if(this.turnName.includes('Outside')){
        this.edge = 'outside'
      }
      else{
        this.edge = 'inside'
      }
      this.turnType = 'Loop'
      this.feet = 'perform on 1 foot'
      this.lobe = 'skate an oval pattern using the same edge, the entry and exit of the loop must cross'
      this.turnDirection = ''
      this.turnPoint = 'loop must be clean cut without scrapes or points'
      this.turnSize = 'loop height 1.5 and width 1 blade lengths'
    }

    if (this.turnName.includes('Rocker')) {
      if(this.turnName.includes('Outside')){
        this.edge = 'outside to outside'
      }
      else{
        this.edge = 'inside to inside'
      }
      this.turnType = 'Rocker'
      this.feet = 'perform on 1 foot'
      this.lobe = 'entry and exit edge on two different lobes'
      this.turnDirection = 'turn same direction as your entry edge (if entry edge is clockwise, turn clockwise)'
      this.turnPoint = 'cusp of turn points into first lobe'
      this.turnSize = ''
    }

    if (this.turnName.includes('Twizzle')) {
      if(this.turnName.includes('Outside')){
        this.edge = ''
      }
      else{
        this.edge = ''
      }
      this.turnType = 'Twizzle'
      this.feet = 'a traveling turn on one foot with one or more rotations'
      this.lobe = ''
      this.turnDirection = ''
      this.turnPoint = 'quickly rotates with a continuous (uninterrupted) action'
      this.turnSize = ''
    }
  }

 openTechnique(value :any){
  if (value == 'head') {
    this.body = "Head"; 
    if(this.turnTitle1Select){
      this.techniqueDescription = this.techniqueArray[0]
    }
    else{
      this.techniqueDescription = this.closedTechniqueArray[0]
    }
    this.openTechniqueAlert()
  }

  if (value == 'skating') {
    this.body = 'Skating Leg'; 
    if(this.turnTitle1Select){
      this.techniqueDescription = this.techniqueArray[2]
    }
    else{
      this.techniqueDescription = this.closedTechniqueArray[2]
    }
    this.openTechniqueAlert()
  }

  if (value == 'upper') {
    this.body = 'Upper Body'; 
    if(this.turnTitle1Select){
      this.techniqueDescription = this.techniqueArray[1]
    }
    else{
      this.techniqueDescription = this.closedTechniqueArray[1]
    }
    this.openTechniqueAlert()
  }

  if (value == 'free') {
    this.body = 'Free Leg'; 
    if(this.turnTitle1Select){
      this.techniqueDescription = this.techniqueArray[3]
    }
    else{
      this.techniqueDescription = this.closedTechniqueArray[3]
    }
    this.openTechniqueAlert()
  }

  if (value == 'weight') {
    this.body = 'Weight on Blade'; 
    if(this.turnTitle1Select){
      this.techniqueDescription = this.techniqueArray[4]
    }
    else{
      this.techniqueDescription = this.closedTechniqueArray[4]
    }
    this.openTechniqueAlert()
  }

  if (value == 'alignment') {
    this.body = 'Alignment'; 
    if(this.turnTitle1Select){
      this.techniqueDescription = this.techniqueArray[5]
    }
    else{
      this.techniqueDescription = this.closedTechniqueArray[5]
    }
    this.openTechniqueAlert()
  }

  if (value == 'tracing') {
    this.body = 'Tracing'; 
    if(this.turnTitle1Select){
      this.turnTracing = this.videoTracing
      this.techniqueDescription = ''
    }
    else{
      this.turnTracing = this.videoTracingClosed
      this.techniqueDescription = ''
    }
    this.openTechniqueAlert()
  }
 }

 async openTechniqueAlert(){
  const modal = await this.modalCtrl.create({
    component: TechniqueComponent,
    cssClass: "modal-fullscreen",
    componentProps: {
      body: this.body,
      techniqueDescription: this.techniqueDescription,
      tracingVideo: this.turnTracing
    }
  });
  return await modal.present();
 }

  async showLoader() {

    const loading = await this.loadingCtrl.create({
      message: '',
    });
    return loading.present();

  }

  async goBack() {
    this.modalCtrl.dismiss({
      'dismissed': true
    })
  }

}
