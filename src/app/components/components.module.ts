import { NgModule } from '@angular/core';
import { QuizComponent } from './quiz/quiz.component';
import { ErrorsComponent } from './errors/errors.component';
import { UnlockComponent } from './unlock/unlock.component';
import { ExercisesComponent } from './exercises/exercises.component';
import { InstructionComponent } from './instruction/instruction.component';
import { TurnSheetComponent } from './turn-sheet/turn-sheet.component';
import { TechniqueComponent } from './technique/technique.component';
import { ResourceComponent } from './resource/resource.component';
import { IntroduceComponent } from './introduce/introduce.component';
import { PlacementComponent } from './placement/placement.component';
import { ReadyComponent } from './ready/ready.component';
import { PlacementDefinitionsComponent } from './placement-definitions/placement-definitions.component';


import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';

import {DragDropModule} from '@angular/cdk/drag-drop'


@NgModule({
	declarations: [
    QuizComponent, ErrorsComponent, UnlockComponent, ExercisesComponent, InstructionComponent, TurnSheetComponent, TechniqueComponent, ResourceComponent, IntroduceComponent, PlacementComponent, ReadyComponent, PlacementDefinitionsComponent],
	imports: [IonicModule, CommonModule, DragDropModule],
	exports: [
    QuizComponent, ErrorsComponent, UnlockComponent, ExercisesComponent, InstructionComponent, TurnSheetComponent, TechniqueComponent, ResourceComponent, IntroduceComponent, PlacementComponent, ReadyComponent, PlacementDefinitionsComponent]
})
export class ComponentsModule {}
