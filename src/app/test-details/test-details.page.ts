import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularFirestore} from '@angular/fire/firestore';
import { AlertController, LoadingController, ModalController, Platform} from '@ionic/angular'
import { Router } from '@angular/router';
import { IAPProduct } from '@ionic-native/in-app-purchase-2/ngx';
import { AuthService } from '../services/auth.service';
import { Location } from "@angular/common";

@Component({
  selector: 'app-test-details',
  templateUrl: './test-details.page.html',
  styleUrls: ['./test-details.page.scss'],
})
export class TestDetailsPage implements OnInit {
  testTitle
  testInfo = []
  collectionName
  patterns = []
  id
  productsPurchased = []
  OFFICIAL_PROD_SELECTED
  price
  quizPage: number
  adultStandardTestTitle
  dBIDName
  quizPreview
  products: IAPProduct[] = [];
  isLoading2 = false;
  //product variable names
  allLevelsID
  allLevelsPrice
  allLevelsOwnedStatus
  allLevelsArray
  quizID
  quizPrice
  quizOwnedStatus
  quizArray

  constructor(
    public route: ActivatedRoute,
    public af: AngularFirestore,
    public alertController: AlertController,
    private router: Router,
    public loadingController: LoadingController,
    public modalCtrl: ModalController,
    private plt: Platform,
    private authService: AuthService,
    private location: Location
  ) { 
    this.authService.passAllLevels$.subscribe((allLevelsID) => {
      console.log('dana in auth service on patterns page');
      this.allLevelsArray = JSON.parse(allLevelsID)
      this.allLevelsID = this.allLevelsArray[0].prodID
      this.allLevelsPrice = this.allLevelsArray[0].price
      this.allLevelsOwnedStatus = this.allLevelsArray[0].owned
      console.log(this.allLevelsID ,this.allLevelsPrice, this.allLevelsOwnedStatus, 'dana this.allLevelsID');
     })
    
    // disable console log
    console.log = function(){};
    this.showLoader().then(() => {
      this.route.queryParams.subscribe((res) => {
        if (res.testTitle != this.testTitle) {
          this.testTitle = res.testTitle
          if ((this.testTitle == 'Adult Intermediate') ||
            (this.testTitle == 'Adult Novice') ||
            (this.testTitle == 'Adult Junior') ||
            (this.testTitle == 'Adult Senior')) {
            this.adultStandardTestTitle = this.testTitle.split('Adult ')
            this.dBIDName = this.adultStandardTestTitle[1]
            this.pullData()
          } else {
            this.dBIDName = this.testTitle
            this.pullData()
          }
        }
      });
    })

    }

    pullData() {
      if(this.plt.is('android')){
        this.OFFICIAL_PROD_SELECTED = 'com.movesofficial.' + (this.dBIDName.replace(/\s/g, "")).replace(/-/g, "").toLowerCase() + 'quiz';
        console.log(this.OFFICIAL_PROD_SELECTED, 'this.OFFICIAL_PROD_SELECTED dana what is this?');
      }
      if(this.plt.is('ios')){
        this.OFFICIAL_PROD_SELECTED = 'com.movesOfficial.' + (this.dBIDName.replace(/\s/g, "")).replace(/-/g, "").toLowerCase() + 'quiz';
        console.log(this.OFFICIAL_PROD_SELECTED, 'this.OFFICIAL_PROD_SELECTED dana what is this?');
      }
      this.af.collection("Tests").doc(this.dBIDName).ref.get().then((doc) => {
        this.testInfo.push(doc.data());
      })

      this.af.collection("Links").doc('previews').ref.get().then((doc) =>{
        this.quizPreview = doc.data()['quiz'];
      });

      this.collectionName = 'Patterns_' + this.dBIDName.replace(/\s/g, "");
      this.af.collection(this.collectionName).ref.orderBy('num').get().then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
          this.patterns.push({
            title: doc.data()['title'],
            id: doc.id
          });
          this.loadingController.dismiss();
        })
      })
    }

    // Show the loader for infinite time
    async showLoader() {

      const loading = await this.loadingController.create({
        message: '',
      });
      return loading.present();

    }

    ngOnInit() {
       //call price and owned variables 
    this.authService.passAllLevels()
    }

    openItem(pattern) {
      this.router.navigate(['tabs/tabs/home/pattern-details'], {
        queryParams: {
          testName: this.testTitle,
          id: pattern.id,
          collectionName: this.collectionName
        },
      });
    }

    async rule5034() {
      const alert = await this.alertController.create({
        header: 'Rule 5034',
        mode: 'ios',
        message: '<p class="alertText"><b>Adult 21+</b> skaters taking adult intermediate through senior tests, should show the same level of achievement as a standard candidate at <u>1 test level below.</u>' +
          ' <b>Adult 50+</b> skaters taking adult intermediate through senior tests, should show the same level of achievement as a standard candidate at <u>2 test levels below.</u>' +
          ' <b>Adult 50+</b> skaters taking adult bronze through gold tests, should show the same level of achievement as an adult 21+ candidate at <u>1 test level below.</u></p>',
        buttons: ['Close']
      });
      await alert.present();
    }

    async present2() {
      this.isLoading2 = true;
      return await this.loadingController.create({
        message: '',
      }).then(a => {
        a.present().then(() => {
          if (!this.isLoading2) {
            a.dismiss().then(() => console.log('abort presenting'));
          }
        });
      });
    }
  
    async dismiss2() {
      this.isLoading2 = false;
      return await this.loadingController.dismiss().then(() => console.log('dismissed'));
    }

    goBack(){
      this.location.back();
    }

    }
