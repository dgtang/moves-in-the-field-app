import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TestDetailsPageRoutingModule } from './test-details-routing.module';

import { TestDetailsPage } from './test-details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TestDetailsPageRoutingModule
  ],
  declarations: [TestDetailsPage]
})
export class TestDetailsPageModule {}
