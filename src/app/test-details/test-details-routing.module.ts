import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TestDetailsPage } from './test-details.page';

const routes: Routes = [
  {
    path: '',
    component: TestDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TestDetailsPageRoutingModule {}
