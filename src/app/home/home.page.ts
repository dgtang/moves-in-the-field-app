import { Component, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { InAppPurchase2, IAPProduct } from '@ionic-native/in-app-purchase-2/ngx';
import { Platform} from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { AngularFirestore} from '@angular/fire/firestore';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage { 
  category = "standard"
  productsPurchased = [];
  allLevelsID = [{ prodID: "com.movesofficial.alllevels", price: " ", owned: false},]
  turnChecklistID = [{ prodID: "com.movesofficial.turnchecklist", price: " ", owned: false},]
  instructionalID = [
    { prodID: "com.movesofficial.prepreliminaryinstructional", price: " ", owned: false},
    { prodID: "com.movesofficial.preliminaryinstructional", price: " ", owned: false},
    { prodID: "com.movesofficial.prejuvenileinstructional", price: " ", owned: false},
    { prodID: "com.movesofficial.juvenileinstructional", price: " ", owned: false},
    { prodID: "com.movesofficial.intermediateinstructional", price: " ", owned: false},
    { prodID: "com.movesofficial.noviceinstructional", price: " ", owned: false},
    { prodID: "com.movesofficial.juniorinstructional", price: " ", owned: false},
    { prodID: "com.movesofficial.seniorinstructional", price: " ", owned: false},
    { prodID: "com.movesofficial.adultprebronzeinstructional", price: " ", owned: false},
    { prodID: "com.movesofficial.adultbronzeinstructional", price: " ", owned: false},
    { prodID: "com.movesofficial.adultsilverinstructional", price: " ", owned: false},
    { prodID: "com.movesofficial.adultgoldinstructional", price: " ", owned: false},
  ]

  constructor(
    private router: Router,
    private cf: ChangeDetectorRef,
    private store: InAppPurchase2,
    private plt: Platform,
    private authService: AuthService,
    public af: AngularFirestore
  ) {

     console.log = function(){};
    this.productsPurchased = JSON.parse(localStorage.getItem('productsPurchased'))
    console.log(this.productsPurchased, 'dana products purchased parsed');
    //if user has not bought the entire app but has previously purchased something 
    if ((this.productsPurchased != null) && (this.productsPurchased != undefined)) {
      if((this.productsPurchased.indexOf(this.allLevelsID) == -1) || (this.productsPurchased != null) || (this.productsPurchased != undefined)){
        this.plt.ready().then(() => {
          console.log('dana in platform home page');  
          this.store.verbosity = this.store.DEBUG;
          this.registerInstructionalContent()
      })
      }
    }
    //if user has not bought the entire app but has NOT previously purchased something 
    if ((this.productsPurchased == null) || (this.productsPurchased == undefined)) {
      this.plt.ready().then(() => {
        console.log('dana in platform home page');  
        this.store.verbosity = this.store.DEBUG;
        this.registerInstructionalContent()
    })
    }
  }


  registerInstructionalContent(){
          //----------------------individual------------------------------
          this.store.register({
            id: this.turnChecklistID[0].prodID,
            type: this.store.NON_CONSUMABLE
          });
    
          this.store.register({
            id: this.allLevelsID[0].prodID,
            type: this.store.NON_CONSUMABLE
          });

           //----------------------instructional------------------------------
          this.store.register({
            id: this.instructionalID[0].prodID,
            type: this.store.NON_CONSUMABLE
          });

          this.store.register({
            id: this.instructionalID[1].prodID,
            type: this.store.NON_CONSUMABLE
          });

          this.store.register({
            id: this.instructionalID[2].prodID,
            type: this.store.NON_CONSUMABLE
          });

          this.store.register({
            id: this.instructionalID[3].prodID,
            type: this.store.NON_CONSUMABLE
          });

          this.store.register({
            id: this.instructionalID[4].prodID,
            type: this.store.NON_CONSUMABLE
          });

          this.store.register({
            id: this.instructionalID[5].prodID,
            type: this.store.NON_CONSUMABLE
          });

          this.store.register({
            id: this.instructionalID[6].prodID,
            type: this.store.NON_CONSUMABLE
          });

          this.store.register({
            id: this.instructionalID[7].prodID,
            type: this.store.NON_CONSUMABLE
          });

          this.store.register({
            id: this.instructionalID[8].prodID,
            type: this.store.NON_CONSUMABLE
          });

          this.store.register({
            id: this.instructionalID[9].prodID,
            type: this.store.NON_CONSUMABLE
          });

          this.store.register({
            id: this.instructionalID[10].prodID,
            type: this.store.NON_CONSUMABLE
          });

          this.store.register({
            id: this.instructionalID[11].prodID,
            type: this.store.NON_CONSUMABLE
          });

          this.store.refresh();
    
          //---------------------grab from store----------------------------
          this.store.ready(() =>  {
             //---------------------individual items----------------------------
            this.turnChecklistID[0].price = this.store.get(this.turnChecklistID[0].prodID).price
            this.turnChecklistID[0].owned = this.store.get(this.turnChecklistID[0].prodID).owned
            console.log(this.turnChecklistID[0].price, this.turnChecklistID[0].owned, 'dana on home page turn checklist query');
            

            this.allLevelsID[0].price = this.store.get(this.allLevelsID[0].prodID).price
            this.allLevelsID[0].owned = this.store.get(this.allLevelsID[0].prodID).owned
            console.log(this.allLevelsID[0].price, this.allLevelsID[0].owned, 'dana on home page allLevelsID query');


             //---------------------instructional items----------------------------
            this.instructionalID[0].price = this.store.get(this.instructionalID[0].prodID).price
            this.instructionalID[0].owned = this.store.get(this.instructionalID[0].prodID).owned
            console.log(this.instructionalID[0].price, this.instructionalID[0].owned, 'dana on home page instructionalID 0 query');


            this.instructionalID[1].price = this.store.get(this.instructionalID[1].prodID).price
            this.instructionalID[1].owned = this.store.get(this.instructionalID[1].prodID).owned
            console.log(this.instructionalID[1].price, this.instructionalID[1].owned, 'dana on home page instructionalID 1 query');


            this.instructionalID[2].price = this.store.get(this.instructionalID[2].prodID).price
            this.instructionalID[2].owned = this.store.get(this.instructionalID[2].prodID).owned
            console.log(this.instructionalID[2].price, this.instructionalID[2].owned, 'dana on home page instructionalID 2 query');

            this.instructionalID[3].price = this.store.get(this.instructionalID[3].prodID).price
            this.instructionalID[3].owned = this.store.get(this.instructionalID[3].prodID).owned
            console.log(this.instructionalID[3].price, this.instructionalID[3].owned, 'dana on home page instructionalID 3 query');

            this.instructionalID[4].price = this.store.get(this.instructionalID[4].prodID).price
            this.instructionalID[4].owned = this.store.get(this.instructionalID[4].prodID).owned
            console.log(this.instructionalID[4].price, this.instructionalID[4].owned, 'dana on home page instructionalID 4 query');

            this.instructionalID[5].price = this.store.get(this.instructionalID[5].prodID).price
            this.instructionalID[5].owned = this.store.get(this.instructionalID[5].prodID).owned
            console.log(this.instructionalID[5].price, this.instructionalID[5].owned, 'dana on home page instructionalID 2 query');

            this.instructionalID[6].price = this.store.get(this.instructionalID[6].prodID).price
            this.instructionalID[6].owned = this.store.get(this.instructionalID[6].prodID).owned
            console.log(this.instructionalID[6].price, this.instructionalID[6].owned, 'dana on home page instructionalID 6 query');

            this.instructionalID[7].price = this.store.get(this.instructionalID[7].prodID).price
            this.instructionalID[7].owned = this.store.get(this.instructionalID[7].prodID).owned
            console.log(this.instructionalID[7].price, this.instructionalID[7].owned, 'dana on home page instructionalID 7 query');

            this.instructionalID[8].price = this.store.get(this.instructionalID[8].prodID).price
            this.instructionalID[8].owned = this.store.get(this.instructionalID[8].prodID).owned
            console.log(this.instructionalID[8].price, this.instructionalID[8].owned, 'dana on home page instructionalID 8 query');

            this.instructionalID[9].price = this.store.get(this.instructionalID[9].prodID).price
            this.instructionalID[9].owned = this.store.get(this.instructionalID[9].prodID).owned
            console.log(this.instructionalID[9].price, this.instructionalID[9].owned, 'dana on home page instructionalID 9 query');

            this.instructionalID[10].price = this.store.get(this.instructionalID[10].prodID).price
            this.instructionalID[10].owned = this.store.get(this.instructionalID[10].prodID).owned
            console.log(this.instructionalID[10].price, this.instructionalID[10].owned, 'dana on home page instructionalID 10 query');

            this.instructionalID[11].price = this.store.get(this.instructionalID[11].prodID).price
            this.instructionalID[11].owned = this.store.get(this.instructionalID[11].prodID).owned
            console.log(this.instructionalID[11].price, this.instructionalID[11].owned, 'dana on home page instructionalID 11 query');

            this.sendDataToService()
          })
  }

  sendDataToService(){
      this.authService.itemsFromStore(
        JSON.stringify(this.allLevelsID), 
        JSON.stringify(this.turnChecklistID), 
        JSON.stringify(this.instructionalID)
        )
  }

  ngOnInit() {   

  }

// put this in rules of databse in firebase to allow write and comment out everything else
// match /{document=**} {
//   allow read, write;
// }
// use below function to change rulebook pages, put this function on home apge and then just have to click on a test on home 
// page and it will loop through patterns and update the pages:)
// put this this.updateDiagramPages(value) in openItem function to call this cfunction
// updateDiagramPages(value)
// {
//   console.log('in update function');
//    var collectionName = 'Patterns_' + value.replace(/\s/g, "");
//    console.log(collectionName, 'collectionName');
//    //loop through patterns 
//      this.af.collection(collectionName).ref.get().then((querySnapshot) => {
//       querySnapshot.forEach((doc) => {
//         if((doc.data()['diagramURL'] != null) && (doc.data()['diagramURL'] != undefined)){
//           //get diagram value and split / and save number 
//         let diagramPageNumber = Number(doc.data()['diagramURL'].split('/')[5]) + 6
//         console.log(diagramPageNumber, 'diagramPageNumber');
//         //add 6 = save this number 
//         let newDiagramURL = 'https://online.flippingbook.com/view/375134/' + diagramPageNumber + '/'
//         console.log(newDiagramURL, 'newDiagramURL');
//         console.log(doc.id, 'doc.id');
//           this.af.collection(collectionName).doc(doc.id).update({
//             diagramURL: newDiagramURL
//           })
//         }
//       });
//     })
// }


  segmentChanged(ev: any) {
    this.cf.detectChanges();
    if(ev.detail.value == "standard"){
      this.category = "standard"
    }
    if(ev.detail.value == "adult"){
      this.category = "adult"
    }
    if(ev.detail.value == "adaptive"){
      this.category = "adaptive"
    }
  }

  openItem(value :any) {
    this.router.navigate(['tabs/tabs/home/test-details'], {
      queryParams: {
        testTitle : value,
        },
      });
}

}
