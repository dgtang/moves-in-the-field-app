import { Component, OnInit } from '@angular/core';
import { AngularFirestore} from '@angular/fire/firestore';
import { ResourceComponent } from '../components/resource/resource.component';
import { ModalController, Platform} from '@ionic/angular'
import { Browser } from '@capacitor/browser';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-connect',
  templateUrl: './connect.page.html',
  styleUrls: ['./connect.page.scss'],
})
export class ConnectPage implements OnInit {
  chiltonPaymentInfo = []
  instagram
  instagramText
  facebook
  facebookText
  youtube
  website
  judgeForm
  skaterChecklist
  judgePoints
  rulebookURL
  rulebookPages = []
  edgeApple
  edgeAndroid
  motivationApple
  motivationAndroid

  constructor(public af: AngularFirestore, public modalCtrl: ModalController, public plt: Platform,  private iab: InAppBrowser,) { 
    this.af.collection("Links").doc('connect').ref.get().then((doc) => {
      this.instagram = doc.data()['instagram'];
      this.instagramText = doc.data()['instagramText'];
      this.facebook = doc.data()['facebook'];
      this.facebookText = doc.data()['facebookText'];
      this.youtube = doc.data()['youtube'];
      this.website = doc.data()['website'];
      this.judgeForm = doc.data()['judgeForm'];
      this.skaterChecklist = doc.data()['skaterChecklist'];
      this.judgePoints = doc.data()['judgePoints'];
      this.edgeApple = doc.data()['edgeApple'];
      this.edgeAndroid = doc.data()['edgeAndroid'];
      this.motivationApple = doc.data()['motivationApple'];
      this.motivationAndroid = doc.data()['motivationAndroid'];
    });

    this.af.collection("Links").doc('Rulebook').ref.get().then((doc) => {
      this.rulebookURL = doc.data()['link'];
    });

    this.af.collection("Rulebook").ref
      .get()
      .then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
          this.rulebookPages.push(doc.data());
        })
      })
      
//change rulebook numbers
    //    this.af.collection("Rulebook").ref.get().then((querySnapshot) => {
    //   querySnapshot.forEach((doc) => {
    //     let firebasePageNumber = doc.data()['link']
    //     let urlPageNumber = Number(firebasePageNumber.split('/')[5])
    //     let urlPageNumberPlus4 = urlPageNumber -2
    //     let OfficialPage = 'https://online.flippingbook.com/view/375134/' + urlPageNumberPlus4 + '/'
    //     console.log(urlPageNumber, urlPageNumberPlus4, 'urlPageNumber');
    //     console.log(OfficialPage, 'OfficialPage');
    //     console.log(doc.id, 'doc.id');
        
    //       this.af.collection("Rulebook").doc(doc.id).update({
    //         link: OfficialPage
    //       })
    //   });
    // })
  }

  ngOnInit() {
console.log('what sup');

}

async openSpecificPage(page){
  if (this.plt.is('ios')) {
    await Browser.open({ url: page.link });
  }
  if (this.plt.is('android')) {
    this.iab.create(page.link, '_blank'); 
  }
}

async openRulebook(){
  if (this.plt.is('ios')) {
    await Browser.open({ url: this.rulebookURL });
  }
  if (this.plt.is('android')) {
    this.iab.create(this.rulebookURL, '_blank'); 
  }
}

async openJudgePoints(){
  if (this.plt.is('ios')) {
    await Browser.open({ url: this.judgePoints });
  }
  if (this.plt.is('android')) {
    this.iab.create(this.judgePoints, '_blank'); 
  }
}

async openSkaterChecklist(){
  if (this.plt.is('ios')) {
    await Browser.open({ url: this.skaterChecklist });
  }
  if (this.plt.is('android')) {
    this.iab.create(this.skaterChecklist, '_blank'); 
  }
}

async openJudgeForm(){
  if (this.plt.is('ios')) {
    await Browser.open({ url: this.judgeForm });
  }
  if (this.plt.is('android')) {
    this.iab.create(this.judgeForm, '_blank'); 
  }
}

async openWebsite(){
  if (this.plt.is('ios')) {
    await Browser.open({ url: this.website });
  }
  if (this.plt.is('android')) {
    this.iab.create(this.website, '_blank'); 
  }
}

async openYoutube(){
  if (this.plt.is('ios')) {
    await Browser.open({ url: this.youtube });
  }
  if (this.plt.is('android')) {
    this.iab.create(this.youtube, '_blank'); 
  }
}

async openFacebook(){
  if (this.plt.is('ios')) {
    await Browser.open({ url: this.facebook });
  }
  if (this.plt.is('android')) {
    this.iab.create(this.facebook, '_blank'); 
  }
}

async openInstagram(){
  if (this.plt.is('ios')) {
    await Browser.open({ url: this.instagram });
  }
  if (this.plt.is('android')) {
    this.iab.create(this.instagram, '_blank'); 
  }
}

async openEdgeApp(){
  if (this.plt.is('ios')) {
    await Browser.open({ url: this.edgeApple });
  }
  if (this.plt.is('android')) {
    this.iab.create(this.edgeAndroid, '_blank'); 
  }
}

async openMotivationApp(){
  if (this.plt.is('ios')) {
    await Browser.open({ url: this.motivationApple });
  }
  if (this.plt.is('android')) {
    this.iab.create(this.motivationAndroid, '_blank'); 
  }
}

async goToResources(value: any) {
  const modal = await this.modalCtrl.create({
    component: ResourceComponent,
    cssClass: "modal-fullscreen",
    componentProps: {
      item: value,
    }
  });
  return await modal.present();
}

// changeData(){
//   console.log('in change dtaa function');
  
//   //top
// //change collection name 
//  this.af.collection("Patterns_Pre-Preliminary").get()
//  .subscribe((querySnapshot) => {
//    console.log('in query snapshot');
   
//    querySnapshot.forEach((doc) => { 

//      console.log('in firebase function');
     
//      this.chiltonPaymentInfo.push(doc.data());
//      console.log(this.chiltonPaymentInfo, this.chiltonPaymentInfo.length, "info and length");
//      //change length 
//      //if (this.chiltonPaymentInfo.length == 4){
//       // for (var i = 0; i < this.chiltonPaymentInfo.length; i++) {
//          //chnage this for each thing increase number by one 
//          var i = 0;
//          console.log(i, "i equals");
         
//          // this.chiltonTotal += this.chiltonPaymentInfo[i].amountLeftOver;
//          console.log(this.chiltonPaymentInfo[i].blade,"this.chiltonPaymentInfo[i].blade" );
//          console.log(this.chiltonPaymentInfo[i].bullets, "this.chiltonPaymentInfo[i].bullets");
//          console.log(this.chiltonPaymentInfo[i].corrections, "this.chiltonPaymentInfo[i].corrections");
//          console.log(this.chiltonPaymentInfo[i].desc, "this.chiltonPaymentInfo[i].desc");
//          console.log(this.chiltonPaymentInfo[i].diagramURL, "this.chiltonPaymentInfo[i].diagramURL");
//          console.log(this.chiltonPaymentInfo[i].errors,"this.chiltonPaymentInfo[i].errors");
//          console.log(this.chiltonPaymentInfo[i].exerciseVideos, "this.chiltonPaymentInfo[i].exerciseVideos");
//          console.log(this.chiltonPaymentInfo[i].exercises, "this.chiltonPaymentInfo[i].exercises");
//          console.log(this.chiltonPaymentInfo[i].focus, "this.chiltonPaymentInfo[i].focus");
//          console.log(this.chiltonPaymentInfo[i].introduction, "this.chiltonPaymentInfo[i].introduction");
//          console.log(this.chiltonPaymentInfo[i].moreInfo, "this.chiltonPaymentInfo[i].moreInfo");
//          console.log(this.chiltonPaymentInfo[i].num, "this.chiltonPaymentInfo[i].num");
//          console.log(this.chiltonPaymentInfo[i].patternAxis, "this.chiltonPaymentInfo[i].patternAxis");
//          console.log(this.chiltonPaymentInfo[i].slowVideos, "this.chiltonPaymentInfo[i].slowVideos");
//          console.log(this.chiltonPaymentInfo[i].techniqueTitles, "this.chiltonPaymentInfo[i].techniqueTitles");
//          console.log(this.chiltonPaymentInfo[i].variations, "this.chiltonPaymentInfo[i].variations");
//          console.log(this.chiltonPaymentInfo[i].videoDesc1, "this.chiltonPaymentInfo[i].videoDesc1");
//          console.log(this.chiltonPaymentInfo[i].videoDesc2, "this.chiltonPaymentInfo[i].videoDesc2");
//          console.log(this.chiltonPaymentInfo[i].youtubeURL1, "this.chiltonPaymentInfo[i].youtubeURL1");
//          console.log(this.chiltonPaymentInfo[i].watchFor, "this.chiltonPaymentInfo[i].watchFor");
//          console.log(this.chiltonPaymentInfo[i].youtubeURL2, "this.chiltonPaymentInfo[i].youtubeURL2");
//          console.log(this.chiltonPaymentInfo[i].title, "this.chiltonPaymentInfo[i].title");
//          console.log(this.chiltonPaymentInfo[i].youtubeURLAdult, "this.chiltonPaymentInfo[i].youtubeURLAdult");
//          console.log(this.chiltonPaymentInfo[i].videoDescAdult, "this.chiltonPaymentInfo[i].videoDescAdult");
//          console.log(this.chiltonPaymentInfo[i].numLobes, "this.chiltonPaymentInfo[i].numLobes");
//          console.log(this.chiltonPaymentInfo[i].numLobesNumber, "this.chiltonPaymentInfo[i].numLobesNumber");
         
         
//          //change this document id name 
//          //ADD ADULT URL 
//         //  this.af.collection("Patterns_Pre-Preliminary").doc("3. Forward Right & Left Foot Spirals").set({
//         // blade: this.chiltonPaymentInfo[i].blade,
//         //  bullets: this.chiltonPaymentInfo[i].bullets,
//         //  corrections: this.chiltonPaymentInfo[i].corrections,
//         //  desc: this.chiltonPaymentInfo[i].desc,
//         //  diagramURL: this.chiltonPaymentInfo[i].diagramURL,
//         //  errors: this.chiltonPaymentInfo[i].errors,
//         //  exerciseVideos: this.chiltonPaymentInfo[i].exerciseVideos,
//         //  exercises: this.chiltonPaymentInfo[i].exercises,
//         //  focus: this.chiltonPaymentInfo[i].focus,
//         //  introduction: this.chiltonPaymentInfo[i].introduction,
//         //  moreInfo: this.chiltonPaymentInfo[i].moreInfo,
//         //  moreTechniqueInfo: this.chiltonPaymentInfo[i].moreTechniqueInfo,
//         //  num: this.chiltonPaymentInfo[i].num,
//         //  patternAxis: this.chiltonPaymentInfo[i].patternAxis,
//         //  slowVideos: this.chiltonPaymentInfo[i].slowVideos,
//         //  techniqueTitles: this.chiltonPaymentInfo[i].techniqueTitles,
//         //  title: this.chiltonPaymentInfo[i].title,
//         //  variations: this.chiltonPaymentInfo[i].variations,
//         //  videoDesc1: this.chiltonPaymentInfo[i].videoDesc1,
//         //  videoDesc2: this.chiltonPaymentInfo[i].videoDesc2,
//         //  watchFor: this.chiltonPaymentInfo[i].watchFor,
//         //  youtubeURL1: this.chiltonPaymentInfo[i].youtubeURL1,
//         //  youtubeURL2: this.chiltonPaymentInfo[i].youtubeURL2,
//         //  youtubeURLAdult: this.chiltonPaymentInfo[i].youtubeURLAdult,
//         //  videoDescAdult: this.chiltonPaymentInfo[i].videoDescAdult,
//         //  numLobes: this.chiltonPaymentInfo[i].numLobes,
//         //  numLobesNumber: this.chiltonPaymentInfo[i].numLobesNumber
//         //  })
//          //21 variables for standard tests ...adult and moreInfo and moreTehcniqueInfo are all things and num lobes and numLobes number
//          //24 variables for adult tests ...only counting 23
//        //}
//     // }
    
//    });
//  });
// //bottom
// }

}


