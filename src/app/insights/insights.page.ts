import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { TurnSheetComponent } from '../components/turn-sheet/turn-sheet.component';

@Component({
  selector: 'app-insights',
  templateUrl: './insights.page.html',
  styleUrls: ['./insights.page.scss'],
})
export class InsightsPage implements OnInit {

  constructor(
    public modalCtrl: ModalController,
  ) { }

  ngOnInit() {
  }

  async openTurnSheet(){    
    const modal = await this.modalCtrl.create({
      component: TurnSheetComponent,
      cssClass: "modal-fullscreen",
      // componentProps: {
      //   title: this.testTitle,
      //   OFFICIAL_PROD_SELECTED: 'com.movesOfficial.' + (this.testTitle.replace(/\s/g, "")).replace(/-/g, "").toLowerCase() + 'quiz',
      //   startQuizForRestore: this.startQuizForRestore
      // }
    });
    return await modal.present();
  }

}
